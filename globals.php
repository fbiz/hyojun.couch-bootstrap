<?php require($_SERVER["DOCUMENT_ROOT"].'/couch/cms.php'); ?>
<cms:embed "html/is-logged.php" />
<cms:embed "/global-vars.php" />
<cms:template title="Configurações gerais" order="1" executable="0">
	<cms:editable name="seo" label="SEO" type="group" />

	<cms:editable name="site_title"
		label="Título do Site"
		desc="<cms:show field_title_desc />"
		type="text"
		group="seo"
		order="0" />

	<cms:editable name="site_description"
		label="Descrição do Site"
		desc="<cms:show field_description_desc />"
		type="text"
		group="seo"
		order="0" />

	<cms:editable name="bi" label="Tags de BI" type="group" />
	<cms:editable name="tracking"
		label="Tracking code"
		desc="Código do Analytics, Tag Manager, etc."
		type="textarea"
		no_xss_check="1"
		group="bi"
		order="5"></cms:editable>
</cms:template>
<?php COUCH::invoke(); ?>

<?php
	if ( !defined('K_COUCH_DIR') ) die(); // cannot be loaded directly

	$tcAuth = new KAuth();

	if ($tcAuth->user->access_level >= K_ACCESS_LEVEL_ADMIN) {
		define( 'TC_DEPLOY', K_ADMIN_URL . 'addons/tc-deploy/' );
		$FUNCS->load_js(TC_DEPLOY . 'tc-deploy.js');
		$FUNCS->load_css(TC_DEPLOY . 'tc-deploy.css');
	}


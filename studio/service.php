<?php

	class Builder{

		private $root;
		private $modules_root;
		private $modules_folder = 'studio/';
		private $meta_builder = 'couch:studio';

		public function __construct(){
			$this->root = $_SERVER["DOCUMENT_ROOT"] . '/';
			$this->modules_root = $this->root . 'includes/' . $this->modules_folder;
		}

		private function on_load() {
			return array(
				"status" => 'success',
				"items" => $this->get_templates( glob($this->root . '*.php') ),
				"modules"=> $this->get_modules( glob($this->modules_root . '*.php') )
			);
		}

		private function on_exclude() {

			$name = $_GET['name'];
			$items = $this->get_templates( glob($this->root . '*.php') );

			foreach($items as $item):
				if( end(explode('/', $item['name'])) == ($name.'.php') ):
					unlink($this->root . $name . '.php');
					return array(
						"status" => 'success',
						"message" => 'template.remove.success'
					);
				endif;
			endforeach;

			return array(
				"status" => 'error',
				"message" => 'template.remove.error'
			);
		}

		private function on_save() {

			$modules = $_GET['modules'];
			$filename = $_GET['template_file'];

			$template = file_get_contents('template.tpl');
			$tags = $this->create_tags( $modules );
			$metatags = $this->create_meta_tags( $modules );
			$html = $this->create_html( $template, $tags, $metatags );

			$result = $this->save_file( $this->root . $filename, $html );
			$saved = $this->get_templates( glob($this->root . '*.php') );

			return array(
				"url" => $this->get_url($filename . '.php'),
				"status" => $result['status'],
				"message"=> $result['message'],
				"items" => $saved
			);
		}

		private function get_url($filename) {
			$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
			$folder = $folder = str_replace($_SERVER["DOCUMENT_ROOT"] . '/', '', $this->root);
			$filename = str_replace($_SERVER["DOCUMENT_ROOT"] . '/', '', $filename);
			return $root . $folder . $filename;
		}

		private function create_meta_tags( $modules ) {
			return "<meta name='" . $this->meta_builder . "' content='" . json_encode($modules) . "' />";
		}

		private function create_html( $template, $tags, $metatags ) {
			$html = str_replace("{content}", $tags, $template);
			$html = str_replace("{meta}", $metatags, $html);
			return $html;
		}

		private function create_tags( $modules ) {

			$includes = "";
			$i = 0;

			foreach( $modules as $item ):
				$includes .= "\n\t\t<cms:set studio_index=\"" . ($i++) . "\" />";
				$includes .= "\n\t\t<cms:set studio_id=\"{$item['id']}\" />";
				$includes .= "\n\t\t<cms:embed \"{$this->modules_folder}{$item['file']}.php\" />\n";
			endforeach;

			return $includes;
		}

		private function is_valid( $filename ){

			$filename = $filename . '.php';

			if( file_exists( $filename ) ):
				$metatags = get_meta_tags( $filename );
				if( array_key_exists($this->meta_builder, $metatags) ):
					return array("success" => true);
				else :
					return array(
						"success" => false,
						"message" => 'template.invalid.name'
					);
				endif;
			endif;

			return array( "success" => true );
		}

		private function save_file( $filename, $html ){

			$validation = $this->is_valid( $filename );

			if( !$validation['success'] ):
				return array(
					"status" => 'error',
					"message" => $validation['message']
				);
			endif;

			try{
				$file = fopen( $filename .'.php', 'w');
				fwrite( $file, $html );
				fclose( $file );

				return array(
					"status" => 'success',
					"message" => 'template.save.success'
				);

			}catch(Exception $e){
				return array( "status" => 'error', "message" =>e.message);
			}
		}

		private function get_modules( $files ){

			$response = array();

				foreach( $files as $f ):

					$file = file_get_contents($f);
					preg_match('/\/\*.*?\@studio\((.*)\).*?\*\//s', $file, $matches);

					if(!$matches[1]):
						return "File .$file is not in a valid json format";
					endif;

					$json = json_decode( $matches[1], true );
					$json['file'] = basename($f, '.php');
					array_push( $response,  $json);

				endforeach;

			return $response;
		}

		private function get_templates( $files ){

			$result = array();

			foreach( $files as $template ):
				$metatags = get_meta_tags( $template );
				$admin_link = '';

				$url = $this->get_url($template);

				if( array_key_exists($this->meta_builder, $metatags) ):
					$modules = json_decode($metatags[$this->meta_builder]);
					array_push($result, array(
							"admin" => "",
							"url" => $url,
							"modules" => $modules,
							"name" => end(explode("/", $template))
						)
					);
				endif;
			endforeach;

			return $result;
		}

		public function controller( $action ){

			$method = 'on_'.$action;
			if(is_callable(array( $this, $method ))):
				return json_encode( $this->{$method}() );
			else:
				return json_encode('Action not found');
			endif;
		}
	}

	$Builder = new Builder();
	echo $Builder->controller( $_GET['action'] );
?>

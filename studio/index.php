<?php require($_SERVER["DOCUMENT_ROOT"].'/couch/cms.php'); ?>
<cms:embed "html/is-logged.php" />
<cms:template hidden="1" order="100" />
<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<title>CouchCMS Studio</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css" type="text/css" />
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/dragula/3.6.3/dragula.min.css" type="text/css" />
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" type="text/css" />
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" type="text/css" />
		<link rel="stylesheet" href="css/app.css" type="text/css" />
		<script type="text/javascript">var jailsapp = 'apps/builder';</script>
		<script type="text/javascript" data-main="js-min/builder" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.1.16/require.min.js" async></script>
	</head>

	<!--@dragscroll({ target :'.gu-mirror' })-->
	<body data-app="builder" data-component="dragscroll" class="loading">
		<div data-controller="notifier"></div>
		<div data-controller="connect-page"></div>
		<header class="header" data-controller="menu" data-service="service.php" data-cloak>
			<nav class="menu" data-component="riot-view">
				<a class="button new" href="#/new" title="Nova página">
					<span class="icon icon-new"></span>
					Novo
				</a>
				<a class="button open {disabled:listed==true}" href="#/open" title="Abrir">
					<span class="icon icon-open"></span>
					Abrir
				</a>
			</nav>

			<div class="page-nav-holder">
				<h1 class="main-title js-title" data-component="riot-view">
					<span class="{unsaved:status=='CHANGED'}">
						{current.name || 'Template Studio'}<span if="{ status == 'CHANGED' }">*</span>
					</span>
				</h1>

				<nav class="menu" data-component="riot-view">
					<a class="button remove {disabled:!showExclude}" title="Remover" href="#/remove" id='{current.name}'>
						<span class="icon icon-remove"></span>
						Excluir
					</a>
					<a class="button duplicate {disabled:!showDuplicate}" title="Duplicar página" href="#/duplicate">
						<span class="icon icon-duplicate"></span>
						Duplicar
					</a>
					<a class="button save {disabled:status!='CHANGED'}" title="Salvar" href="#/save">
						<span class="icon icon-save"></span>
						Salvar
					</a>
				</nav>
			</div>

			<nav class="menu admin-menu" data-component="riot-view">
				<a class="button preview {disabled:!current.admin}" title="Administrar" href="{current.admin ? current.admin : '#/admin'}" target="_blank">
					<span class="icon icon-admin"></span>
					Administrar
				</a>
				<a class="button preview {disabled:!showPreview}" title="Visualizar" href="{current.url ? current.url : '#/preview'}" target="_blank">
					<span class="icon icon-preview"></span>
					Visualizar
				</a>
			</nav>
		</header>

		<section class="app" data-controller="dragndrop" data-cloak>

			<aside class="sidebar sidebar-modules" data-controller="modules">
				<h1 class="text-left title">
					Módulos
				</h1>

				<div class="view"
					id="modules"
					data-component="riot-view"
					data-template="#item">
				</div>
			</aside>

			<aside class="sidebar sidebar-page-list" data-controller="templates">
				<h1 class="title">
					Páginas
				</h1>
				<!--@filter({ min: 1 })-->
				<input class="search" type="text" data-component="filter" placeholder="Filtrar página">
				<section class="view" data-component="riot-view">
					<em class="filter-no-results" show="{!templates.length}">Nenhuma página encontrada.</em>
					<div class="list-group" show="{templates.length}">
						<a each="{templates}" href="#" class="list-group-item {active:name==current.name}">
							{name}
						</a>
					</div>
				</section>
			</aside>

			<main class="editor-wrapper" data-controller="editor">
				<section class="editor">
					<div class="editor-message">
						Arraste os módulos ao lado
					</div>
					<section
						class="editor-content" id="area"
						data-component="riot-view"
						data-template="#item">
					</section>
				</section>
			</main>
		</section>

		<script type="text/template" id="item">
			<em class="modules-no-results" show="{!!show}" >Nenhum módulo encontrado.</em>
			<a href="#" class="thumbnail" each="{items}" id="{id}" data-file="{file}">
				<span class="remove button">
					<span class="icon icon-remove"></span>
					Excluir
				</span>
				<img riot-src="{image}" />
				<span class="thumb-label">
					{label}
				</span>
			</a>
		</script>
	</body>
</html>
<?php COUCH::invoke(); ?>

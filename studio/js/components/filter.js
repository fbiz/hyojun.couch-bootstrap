define([

	'jails'

], function( jails ){

	return jails.component('filter', function( html, anno ){

		var min, cp = this;

		this.init = function(){
			min = anno.min;
			this.on('keyup', on_keyup);
		};

		function on_keyup(e){
			var value = e.target.value;

			if( value.length >= min ) {
				cp.emit('action', value);
			} else {
				cp.emit('neutral');
			}
		}
	});
});

define([

	'jails'

], function( jails ){

	return jails.component('dragscroll', function(html, anno){

		var clicked = false,
			docEl = document.documentElement,
			viewheight = parseInt(window.getComputedStyle(html).height, 10);

		this.init = function(){
			jails.events.on(document, 'mousedown', mousedown);
			jails.events.on(document, 'mouseup', mouseup);
		};

		function mousedown(e){
			jails.events.off(document, 'mousemove', mousemove);
			jails.events.on(document, 'mousemove', mousemove);
			clicked = true;
		}

		function mouseup(){
			jails.events.off(document, 'mousemove', mousemove);
			clicked = false;
		}

		function mousemove(e){
			if( clicked ) update(e);
		}

		function update(e){

			var top,
				element = document.querySelector( anno.target );

			if( element ){

				top = e.clientY - 60;
				height = 120;
				bottom = top + height;
				offsetTop = offset(html).top;

				if( top < offsetTop ){
					html.scrollTop -= Math.abs(top - offsetTop);
				}
				else if( bottom > offsetTop + viewheight ){
					html.scrollTop += Math.abs(bottom - (offsetTop + viewheight));
				}
			}
		}

		function offset(el){
			var rect = el.getBoundingClientRect();
			return {
				top: rect.top + document.body.scrollTop,
				left: rect.left + document.body.scrollLeft
			};
		}
	});
});

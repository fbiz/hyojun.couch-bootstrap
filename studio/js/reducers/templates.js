define([
	'modules/template-map'
], function(map){

	return function( state, action ){

		state = state || [];

		switch( action.type ){

			case 'EXCLUDE': return state.filter(function(item){
				return item.name !== action.name;
			});

			case 'LOAD':
				return map(action.templates, action.modules);

			default:
				return state;
		}

	};
});

define(function(){

	return function( state, action ){

		state = state || '';

		switch( action.type ){

			case 'ADD':
			case 'UPDATE':
			case 'REMOVE':
				return 'CHANGED';

			case 'COUCH_CONNECT':
			case 'DUPLICATE':
			case 'NEW':
			case 'LOAD':
			case 'EXCLUDE':
			case 'LOAD_TEMPLATE':
				return action.type;

			default:
				return state;
		}

	};
});

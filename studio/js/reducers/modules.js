define(function(){

	return function( state, action ){

		state = state || [];

		switch( action.type ){

			case 'LOAD':
				return action.modules;

			default : return state;
		}

	};
});

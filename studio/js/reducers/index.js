define([

	'reducers/items',
	'reducers/templates',
	'reducers/modules',
	'reducers/current',
	'reducers/status'

], function( items, templates, modules, current, status ){

	return function( state, action ){

		return {
			items: items( state.items, action ),
			templates: templates( state.templates, action ),
			modules: modules( state.modules, action ),
			current: current( state.current, action ),
			status: status( state.status, action )
		};
	};
});

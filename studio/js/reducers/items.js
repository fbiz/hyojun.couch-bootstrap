define(function(){
	return function( state, action ){

		state = state || [];

		switch( action.type ){

			case 'ADD':
				return add( state, create(action), action );

			case 'UPDATE':
				var item = state.filter(function(i){ return +i.id === +action.id; })[0],
					newstate = state.filter(function(i){ return +i.id !== +action.id; });
				return item ? add(newstate, item, action) : state;

			case 'REMOVE': return state.filter(function(item){
				return +item.id !== +action.id;
			});

			case 'LOAD_TEMPLATE':
				var template = action.templates.filter(function(item){
					return item.name === action.name;
				})[0];

				return (template || {}).modules || [];

			case 'EXCLUDE':
				if( !action.current.name ) {
					return [];
				}

				if (action.name === action.current.name) {
					return [];
				}

				return state;

			case 'NEW':
				return [];

			default:
				return state;
		}
	};

	function create( action ) {
		return {
			id: action.id || (Math.random() * Math.pow(10, 20)),
			image: action.image,
			file: action.file
		};
	}

	function add( state, newitem, action ){
		if(!action.position){
			return state.concat( newitem );
		} else{
			return state.reduce(function( acc, item ){
				if( +item.id === +action.position ){
					acc.push( newitem );
				}
				acc.push( item );
				return acc;
			}, []);
		}
	}

});

define(['modules/template-map'], function(map){

	return function( state, action ){
		state = state || {};

		switch( action.type ){

			case 'COUCH_CONNECT':
				return Object.assign(state, {admin: action.admin });

			case 'NEW':
				return action.name ? { name: action.name } : state;

			case 'EXCLUDE':
				return state.name === action.name ? {} : state;

			case 'LOAD':
			case 'LOAD_TEMPLATE':
			case 'PRE_DUPLICATE':
			case 'DUPLICATE':
				var page = null;
				var newState;

				if (action.templates && action.modules) {
					page = map(action.templates, action.modules);
					page = page.filter(function(item){
						return item.name === action.name;
					})[0];
				}

				newState = page || state;
				state.name = action.name || state.name;
				return newState;

			default:
				return state;
		}

	};
});

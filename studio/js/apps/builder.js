define([

	'jails',
	'stores/items',
	'language',
	'npm/reqwest/reqwest.min',
	'mods/routr/routr',
	'npm/imagesloaded/imagesloaded.pkgd.min',
	'bundles/controllers'

], function( jails, store, language, http, routr, imagesloaded ){

	jails.app('builder', function( html, data ){

		var self = this;

		this.init = function(){
			load()
				.then( dispatch )
				.fail( error )
				.then( startRoutes );
			jails.events.on( window, 'beforeunload', ask_for_changes );

		};

		function loaded(){
			html.classList.remove('loading');
		}

		function load(){
			return http({
				url: 'service.php',
				type: 'json',
				data: [{ name:'action', value:'load' }]
			});
		}

		function startRoutes() {
			routr()
				.get('/open/:page', function(page) {
					jails.publish('template:open', page);
				})
				.run()
				.watch();
		}

		function dispatch( response ) {

			store.dispatch({
				type: 'LOAD',
				templates: response.items,
				modules: response.modules
			});

			if (!location.hash.match(/^#\/open\/(.*)?$/)) {
				store.dispatch({
					type: 'NEW',
					name: language['new.template.default']
				});
			}

			imagesloaded( modules, loaded );
		}

		function error(response, error) {
			console.error('Ajax error: ', response, error);
			self.publish('ajax:done', {
				status: 'error',
				message: 'ajax.error.load'
			});
		}

		function ask_for_changes(e){

			var status = store.getState().status;

			if( status.match('CHANGED') ){
				(e || window.event).returnValue = language['alert.leave.changes'];
				return language['alert.leave.changes'];
			}
		}
	});
});

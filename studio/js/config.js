require.config({

	baseUrl :'js/',

	deps: [ 'jails', 'logger', jailsapp ],
	include: ['config'],

	paths: {
		jails: '../node_modules/jails-js/source/jails.min',
		mods: '../node_modules/jails-modules',
		comps: '../node_modules/jails-components',
		logger: '../node_modules/jails-modules/logger/logger',
		riot: '../node_modules/riot/riot.min',
		sweetalert: '../node_modules/sweetalert/dist/sweetalert.min',
		npm: '../node_modules'
	},

	callback: function( jails, logger ){
		logger();
		jails.start();
	}
});

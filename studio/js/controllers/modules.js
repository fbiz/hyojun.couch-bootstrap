define([

	'jails',
	'stores/items',
	'comps/riot-view/riot-view'

], function( jails, store ){

	jails.controller('modules', function( html, data ){

		var self = this, view;

		this.init = function(){
			view = this.x('.view');
			html.querySelector('.modules-no-results');
			store.subscribe( update );
		};

		function update(){
			var modules = store.getState().modules;
			view('update', { items: modules, show: !modules.length });

			if (!modules.length) {
				self.publish('modules:empty');
			}
		}

	});
});

define([
	'jails',
	'stores/items',
	'npm/reqwest/reqwest.min',
	'comps/riot-view/riot-view',
	'components/dragscroll'
], function( jails, store, http ){

	jails.controller('editor', function( html, data ){

		var self = this;
		var editor;
		var view;
		var message;

		this.init = function(){

			view = this.x('#area');
			message = html.querySelector('.editor-message');

			editor = html.querySelector('.editor');
			store.subscribe(update);
		};

		function update(){

			var state = store.getState();

			message.style.display = !!state.items.length ? 'none' : 'block';

			if (state.current.name) {
				editor.classList.add('active');
			} else {
				editor.classList.remove('active');
			}

			view('update', state);
		}
	});
});

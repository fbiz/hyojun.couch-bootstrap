define([

	'jails',
	'stores/items',
	'language',
	'npm/reqwest/reqwest.min',
	'components/filter',
	'comps/riot-view/riot-view'

], function( jails, store, language, http ){

	jails.controller('templates', function( html, data ){

		var self = this, view;

		this.init = function() {

			view = this.x('.view');
			store.subscribe( update );
			self.subscribe('list', showList);
			self.subscribe('template:open', openPage);

			this.on('click', '.list-group-item', select);

			this.listen('filter:neutral', update);
			this.listen('filter:action', filter);
		};

		function update() {
			view('update', store.getState());
		}

		function showList() {
			html.classList.add('show');
			setTimeout(function() {
				jails.events.off(document.body, 'click', onClickOutside);
				jails.events.on(document.body, 'click', onClickOutside);
			}, 0);
		}

		function hideList() {
			jails.events.off(document.body, 'click', onClickOutside);
			html.classList.remove('show');
			self.publish('unlist');
		}

		function onClickOutside(e) {
			var el = e.target;
			var isOutside = true;
			while (el) {
				if (el === html) {
					isOutside = false;
					break;
				}
				el = el.parentNode;
			}

			if (isOutside) {
				hideList();
			}
		}

		function openPage(name) {
			var state = store.getState();
			store.dispatch({
				type: 'LOAD_TEMPLATE',
				name: name,
				modules: state.modules,
				templates: state.templates
			});
		}

		function select() {

			var dialog,
				state = store.getState(),
				name = this.innerHTML;

			function dialogCallback(dialog) {
				if ( dialog || state.status !== 'CHANGED' ) {
					hideList();
					openPage(name);
				}
			}

			if (state.status === 'CHANGED' && state.current.name) {
				swal({
					title: language['alert.title.warn'],
					text: language['alert.leave.changes'],
					type: 'warning',
					showCancelButton: true,
					showConfirmButton: true,
				}, dialogCallback);
			} else {
				dialogCallback(true);
			}

		}

		function filter(e, value) {

			var filtered, templates;

			templates = store.getState().templates;
			filtered = templates.filter(function(item){
				return !!item.name.match(new RegExp(value, 'ig'));
			});

			view('update', { templates : filtered });
		}

	});
});

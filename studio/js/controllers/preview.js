define([

	'jails',
    'stores/items',
	'comps/riot-view/riot-view',

], function( jails, store ){

	jails.controller('preview', function( html, data ){

		var self = this,
			isclosed = false;

		this.init = function(){

			view = this.x('.preview-content');

			this.on('click', '.preview-close', close);
			this.on('animationend', hide);

			jails.events.on(document.body, 'keyup', esc);
			this.subscribe('preview', open);
		};

		function update(){
			view('update', store.getState());
		}

		function open(){
			update();
			html.classList.remove('hide');
			html.classList.add('zoomIn');
		}

		function close(){
			isclosed = true;
			html.classList.remove('zoomIn');
			html.classList.add('zoomOut');
		}

		function esc(e){
			if(e.which == 27){
				close();
			}
		}

		function hide(){
			if(isclosed){
				html.classList.add('hide');
				html.classList.remove('zoomOut');
				isclosed = false;
			}
		}

	});
});

define([
	'jails',
	'language',
	'stores/items',
	'modules/verify',
	'npm/reqwest/reqwest.min',
	'comps/riot-view/riot-view',
	'sweetalert'
], function( jails, language, store, verify, http ){

	jails.controller('menu', function( html, data ){

		var self = this,
			buttons = {},
			status = {
				creationType: null
			},
			service,
			view = this.x('.menu'),
			view_tabs = this.x('.js-title');

		this.init = function(){

			service = html.getAttribute('data-service');
			buttons.save = html.querySelector('.save');
			buttons.duplicate = html.querySelector('.duplicate');

			this.on('click', '.save', save);
			this.on('click', '.new', verify(create));
			this.on('click', '.open', list);
			this.on('click', '.duplicate', verify(duplicate, true));
			this.on('click', '.preview', preview);
			this.on('click', '.remove', exclude);

			self.subscribe('unlist', updateListButton);
			store.subscribe( update );
			update();
		};

		function update(){
			var state = store.getState();

			switch (state.status) {
				case 'NEW':
				case 'LOAD_TEMPLATE':
				case 'DUPLICATE':
					status.creationType = state.status;
					break;
			}

			state.showDuplicate =
			state.showExclude =
			state.showPreview = (status.creationType !== 'NEW');

			view('update', state);
			view_tabs('update', state);
		}

		function preview(e) {
			if (this.classList.contains('disabled')) {
				e.preventDefault();
			}
		}

		function create(e) {
			e.preventDefault();
			store.dispatch({
				type: 'NEW',
				name: language['new.template.default']
			});
		}

		function list(e) {
			e.preventDefault();
			if (this.classList.contains('disabled')) {
				return;
			}
			view('update', {listed: true});
			self.publish('list');
		}

		function updateListButton() {
			view('update', {listed: false});
		}

		function duplicate(e) {
			e.preventDefault();
			if (this.classList.contains('disabled')) {
				return;
			}

			swal({
				title: language['alert.title.duplicate'],
				text: language['new.template'],
				type: "input",
				showCancelButton: true,
				confirmButtonText: language['confirm.ok'],
				closeOnConfirm: true,
				cancelButtonText: language['confirm.cancel']
			}, function(name){
				if (!name) {
					return;
				}
				store.dispatch({
					type: 'PRE_DUPLICATE',
					name: name
				});
				save();
			});
		}

		function save(e) {
			var _this = this,
				state = store.getState(),
				name = state.current.name,
				disabled = e ? this.classList.contains('disabled') : false;

			if (e) {
				e.preventDefault();
			}

			if (disabled) {
				return;
			}

			if (name === language['new.template.default']) {
				swal({
					title: language['alert.title.save'],
					text: language['new.template'],
					type: "input",
					showCancelButton: true,
					confirmButtonText: language['confirm.ok'],
					closeOnConfirm: true,
					cancelButtonText: language['confirm.cancel'],
					showLoaderOnConfirm: true
				}, function(name){
					if (!name) {
						return;
					}
					store.dispatch({
						type: 'PRE_DUPLICATE',
						name: name
					});
					save();
				});
				return;
			}

			http({
				url: service,
				type: 'json',
				data: get_modules(state.items, name)
			})
			.then(function(response) {
				response.modules = response.modules || state.modules;
				save_callback(response, name);
			})
			.fail(function(response, error) {
				console.error('Ajax error: ', response, error);
				self.publish('ajax:done', {
					status: 'error',
					message: 'ajax.error.save'
				});
			});
		}

		function save_callback(response, name){
			if (response.status === 'success') {
				store.dispatch({
					type: 'DUPLICATE',
					name: name,
					modules: response.modules,
					templates: response.items
				});

				store.dispatch({
					type: 'LOAD',
					templates: response.items,
					modules: response.modules
				});
				self.publish('ajax:done', response);
			} else {
				rename(response);
			}
		}

		function exclude(e) {
			e.preventDefault();
			if (this.classList.contains('disabled')) {
				return;
			}

			var name = this.id;

			if (!name) {
				return;
			}

			swal({
				title: language['alert.title.remove'],
				text: language['alert.text.remove.first'] + name + language['alert.text.remove.final'],
				type: "input",
				showCancelButton: true,
				confirmButtonText: language['confirm.ok'],
				closeOnConfirm: false,
				cancelButtonText: language['confirm.cancel'],
				showLoaderOnConfirm: true
			}, function(is_ok){
				if (is_ok === false) {
					return;
				}

				if (is_ok === name) {
					http({
						url: 'service.php',
						type: 'json',
						data: [
							{ name:'action', value:'exclude' },
							{ name:'name', value:name }
						]
					})
					.then(function( response ){
						return http({
							url: 'couch-remove.php',
							type: 'get',
							data: { page: name }
						})
						.then(function() {
							store.dispatch({
								type: 'EXCLUDE',
								name: name,
								current: store.getState().current
							});
							self.publish('ajax:done', response);

							store.dispatch({
								type: 'NEW',
								name: language['new.template.default']
							});
						});
					})
					.fail(function(response, error) {
						console.error('Ajax error: ', response, error);
						self.publish('ajax:done', {
							status: 'error',
							message: 'ajax.error.exclude'
						});
					})
				} else {
					swal({
						title: language['alert.title.error'],
						text: language['alert.text.remove.error'],
						type: 'error'
					});
				}
			});
		}

		function normalizeName(name) {
			return name
				.replace(/[áàãâ]/g, 'a')
				.replace(/[é]/g, 'e')
				.replace(/[í]/g, 'i')
				.replace(/[óõô]/g, 'o')
				.replace(/[ú]/g, 'u')
				.replace(/[ç]/g, 'c')
				.replace(/[\W]/g, '-')
				.replace(/\-+/g, '-')
				.toLowerCase();
		}

		function get_modules( items, name ) {
			return items.map(function(item, i){
				return { name: 'modules[' + i + '][file]', value: item.file };
			})
			.concat({ name: 'template_name', value: name })
			.concat({ name: 'template_file', value: normalizeName(name) })
			.concat({ name: 'action',  value: 'save'})
			.concat(items.map(function(item, i){
				return { name: 'modules[' + i + '][id]', value: item.id };
			}));
		}

		function rename( response ){
			if (response.message === 'template.invalid.name') {
				swal({
					title: language['alert.title.warn'],
					text: language[response.message],
					type: "input",
					showCancelButton: true,
					confirmButtonText: language['confirm.ok'],
					closeOnConfirm: false,
					cancelButtonText: language['confirm.cancel']
				}, function(newname){
					if (newname === false) {
						return;
					}

					store.dispatch({
						type: 'PRE_DUPLICATE',
						name: newname
					});
					save();
				});
			}
		}
	});
});

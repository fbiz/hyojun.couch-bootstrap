define([

	'jails',
	'stores/items',
	'npm/reqwest/reqwest.min',
	'npm/dragula/dist/dragula.min',
	'comps/riot-view/riot-view'

], function( jails, store, http, dragula  ){

	return jails.controller('dragndrop', function( html ){

		var self = this;
		var view = this.x('#area');
		var modules = document.querySelector('#modules');
		var area = document.querySelector('#area');
		var drake;

		this.init = function(){

			self.subscribe('modules:empty', destroyDragula);

			drake = dragula(
				[ modules, area ],
				{
					copy: copy,
					accepts: accepts,
					moves: moves
				}
			);

			drake.on('drop', drop);
			this.on('click', '.remove', remove);
		};

		function destroyDragula() {
			drake.destroy();
			self.off('click');
		}

		function copy( el, source ){
			return source === modules;
		}

		function accepts( el, target ){
			return target !== modules;
		}

		function moves(){
			return !!store.getState().current.name;
		}

		function update( el, sibling ){
			store.dispatch({
				type: 'UPDATE',
				id: +el.id,
				position: sibling ? +sibling.id : null
			});
		}

		function add( el, sibling ){
			store.dispatch({
				type: 'ADD',
				id: +el.id,
				image: el.querySelector('img').getAttribute('src'),
				file: el.getAttribute('data-file'),
				position: sibling ? +sibling.id : null
			});
		}

		function remove(){
			var parent = this.parentNode,
				id = parent.id;
			store.dispatch({
				type: 'REMOVE',
				id: +id
			});
		}

		function drop( el, target, source, sibling ){
			if (el.parentNode != area) {
				return;
			}

			el.classList.remove('compact');

			if (target != modules && target != area) {
				return;
			}

			if (source == modules) {
				el.id = (Math.random() * Math.pow(10, 20));
				add(el, sibling);
				area.removeChild(el);
			} else if(source == area) {
				update(el, sibling);
			}
		}
	});
});

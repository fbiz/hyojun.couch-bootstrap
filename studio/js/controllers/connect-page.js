define([
	'jails',
	'stores/items',
	'npm/reqwest/reqwest.min'
], function( jails, store, http ){

	jails.controller('connect-page', function( html, data ){

		var self = this;

		this.init = function(){
			this.subscribe('ajax:done', connect);
			store.subscribe(onUpdateStore);
		};

		function connect( response ){
			if (response && response.url) {
				http({
					url: response.url,
					type: 'get'
				}).then(updateAdminLink);
			}
		}

		function onUpdateStore() {
			var state = store.getState();
			if (state.status === 'LOAD_TEMPLATE' && !state.current.admin) {
				connect({ url: state.current.url });
			}
		}

		function updateAdminLink(response) {
			var html = document.createElement('html');
			html.innerHTML = response.responseText;
			var meta = html.querySelector('meta[name="couch:adm_link"]')

			if (meta) {
				store.dispatch({
					type: 'COUCH_CONNECT',
					admin: meta.getAttribute('content')
				});
			}
		}
	});
});

define([

	'jails',
	'stores/items',
	'language',
	'comps/riot-view/riot-view',
	'sweetalert'

], function( jails, store, language ){

	jails.controller('notifier', function( html, data ){

		var self = this;

		this.init = function(){
			this.subscribe('ajax:done', notify);
		};

		function notify( response ){
			swal({
				title: language['alert.title.' + response.status],
				text: language[response.message],
				type: response.status,
				showConfirmButton: !(response.status === 'success'),
				timer: response.status === 'success' ? 2000 : null
			});
		}
	});
});

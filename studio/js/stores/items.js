define([

	'mods/reduxtore/reduxtore',
	'reducers/index'

], function( Reduxtore, reducer ){

	return new Reduxtore( reducer, {
		status: '',
		current: {},
		items: [],
		templates: [],
		modules: []
	});

});

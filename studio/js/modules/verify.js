define([

	'stores/items',
	'language'

], function( store, language ){

	return function(fn, keep){

		return function(){

			var disabled = !!(this.classList && this.classList.contains('disabled'));
			var args = arguments;
			var dialog;
			var status = store.getState().status;

			if( !disabled && status === 'CHANGED' ){
				swal({
					title: language['alert.title.warn'],
					text: language['alert.leave.changes'],
					type: 'warning',
					showCancelButton: true,
					showConfirmButton: true,
					closeOnConfirm: !keep
				}, function(dialog) {
					if( dialog ){
						fn.apply( this, args );
					}
				});

			} else {
				fn.apply(this, args);
			}
		};
	};
});

define(function() {
	return function(templates, modules) {
		if (templates && templates.length) {
			return templates.map(function(tpl) {
				return {
					url: tpl.url,
					admin: tpl.admin,
					name: tpl.name.split(/\./).shift(),
					modules: tpl.modules.map(function(data) {

						var module = modules.filter(function(m) {
							return m.file === data.file;
						})[0];

						if (!module) {
							return {};
						}

						return {
							id: data.id,
							image: module.image,
							file: module.file
						};
					})
				};
			});
		}
		return [];
	}
});
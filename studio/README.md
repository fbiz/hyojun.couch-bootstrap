# Instructions

## Setup
    npm install

### Building js
    npm run build:js

### Watching js
    npm run watch:js

## CSS
For simplicity's sake, OLD SCHOOL.

#### LOCAL
- app.js

#### CDN
- animate.css ( Animation tool )
- dragula.css ( Drag'n Drop lib )
- bootstrap.css ( Grid )

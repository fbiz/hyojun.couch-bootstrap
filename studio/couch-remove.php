<?php require($_SERVER["DOCUMENT_ROOT"].'/couch/cms.php'); ?>
<cms:embed "html/is-logged.php" />
<cms:template hidden="1" executable="0" order="100" />

<!-- cms:db_delete masterpage="teste-1.php" / -->

<cms:php>
	global $CTX;
	$CTX->set('page_name', $_GET['page'] . '.php');
</cms:php>

<cms:set page_id="" "global" />
<cms:pages masterpage=page_name>
	<cms:set page_id=k_template_id "global" />
</cms:pages>

<cms:php>
	global $FUNCS, $CTX;

	$nonce = $FUNCS->create_nonce('delete_tpl_' . $CTX->get('page_id'));
	$CTX->set('action_nonce', $nonce);
</cms:php>

<cms:redirect url="<cms:concat k_admin_link 'ajax.php?act=delete-tpl&tpl=' page_id '&nonce=' action_nonce />" />

<?php COUCH::invoke(); ?>

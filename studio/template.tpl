<?php require($_SERVER["DOCUMENT_ROOT"].'/couch/cms.php'); ?>
<cms:embed "html/tag-html.php" />
<cms:template order="25" />
<head>
	<cms:embed "admin/builder-header.php" />
	<cms:embed "html/tag-head.php" />
	<meta name="couch:adm_link" content="<cms:admin_link />">
	{meta}
</head>
<body>
<cms:if is_share == "0">
	<cms:embed "structure/header.php" />

	{content}

	<cms:embed "structure/footer.php" />
	<cms:embed "html/tag-foot.php" />
</cms:if>
</body>
</html>
<?php COUCH::invoke(); ?>

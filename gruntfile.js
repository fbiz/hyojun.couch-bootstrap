module.exports = function(grunt) {

	'use strict';

	require('jit-grunt')(grunt, {
		scsslint: 'grunt-scss-lint'
	});

	if (grunt.option('verbose')) {
		require('time-grunt')(grunt);
	}

	grunt.config.set('paths', {
		rjs: {
			bower: '../../bower_components'
		}
	});

	// tasks estão separadas no diretório grunt
	grunt.loadTasks('grunt');

	grunt.registerTask('setup',
		'Faz o startup da máquina, deixando-a disponível em http://localhost:1144/;',
		[
			'githooks', 'exec:bower', 'exec:bundle',
			'exec:vm-start', 'exec:vm-mysql',
			'update-config', 'css', 'js',
			'sync', 'couch-install', 'exec:vm-restart-apache', 'build'
		]);

	grunt.registerTask('lint',
		'Valida o projeto com jshint e scsslint;',
		['scsslint', 'jshint']);

	grunt.registerTask('default',
		'Task padrão gera o css e js no modo "produção".',
		['css', 'js']);
};

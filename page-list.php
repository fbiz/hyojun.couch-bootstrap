<?php require($_SERVER["DOCUMENT_ROOT"].'/couch/cms.php'); ?>
<cms:embed "html/is-logged.php" />
<cms:template title="Lista de páginas" hidden="1" executable="0" order="100" />
<cms:if is_share == "0">
	<cms:templates order='asc' show_hidden='1'>
page:<cms:show k_template_link />
	<cms:pages masterpage=k_template_name>
		<cms:if k_is_page == '1'>
page:<cms:show k_page_link />
			</cms:if>
		</cms:pages>
	</cms:templates>
</cms:if>
<?php COUCH::invoke(); ?>

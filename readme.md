# Hyojun.couch-bootstrap

`versão 0.3.1`

Bootstrap para projetos estáticos com CMS. Utilizando CouchCMS (php + mysql) utilizados pela F.biz.

## Requisitos mínimos

Instale os seguintes programas:

1. [Ruby](http://www.ruby-lang.org/pt/downloads/) — `2.2` ou `brew install ruby`* ou via rvm `curl -sSL https://get.rvm.io | bash -s stable --ruby`;
2. [Vagrant](https://www.vagrantup.com/downloads.html) — `1.7` ou `brew cask install vagrant`*;
3. [Virtual Box](https://www.virtualbox.org/wiki/Downloads) — `5.0` ou `brew cask install virtualbox`*;
4. [NodeJS](http://nodejs.org) — `0.12` ou `brew install nodejs`*;
5. Bundler `1.10` — `gem install bundler -v '~>1.10'`;
6. Bower `1.6` — `npm install -g bower@^1.6`;
7. GruntCLI `0.1` — `npm install -g grunt-cli@^0.1`;
8. NPM `3.3` — `npm install -g npm@^3.3`;

_*No caso do Mac com [homebrew](http://brew.sh/) + [cask](http://caskroom.io/) instalado._

> ### Instalação no Windows
> 
> Além das dependências acima, instale o rSync para Windows (https://www.itefix.net/content/cwrsync-free-edition) e coloque no PATH do sistema. Garanta que estão antes do PATH do GIT para que não haja conflitos na sincronização.
> 
> **Importante**
> 
> 1. O processo funciona apenas pelo CMD/Console2, o rSync dá conflito no GitBash;
> 2. Atenção com o NodeJS, ao instalar uma nova versão certifique-se de baixar na mesma arquitetura já instalada: 32 ou 64bits;
> 3. Caso tenha o HyperV instalado, garanta que o mesmo não esteja rodando ao mesmo tempo do Virtualbox, isso gera conflito de hardware e a máquina não será provisionada.

> ### Utilizando NPM 2.x
> 
> Este projeto utiliza o arquivo `npm-shrinkwrap.json` para forçar versões fechadas das sub-dependências dos projetos, evitando instabilidade nos builds (principalmente no caso do imagemin).
> 
> Existem 2 arquivos, `npm-shrinkwrap-2.0.json` e `npm-shrinkwrap-3.0.json`. Caso esteja utilizando o NPM 2, copie o arquivo:
> 
>     $ cp npm-shrinkwrap{-2.0,}.json && npm install

Clone o projeto e rode as tarefas:

    $ git clone git@bitbucket.org:fbiz/hyojun.couch-bootstrap.git
    $ cd hyojun.couch-bootstrap/ && npm install && grunt setup

Quando a máquina virtual estiver pronta, o servidor estará disponível em http://localhost:1144/

Para acessar o admin do CouchCMS, entre em http://localhost:1144/couch/adm.php com o login `system` e senha `admin`.

> [![Screencast instalação](https://i.ytimg.com/vi/I4V-yftdFvY/hqdefault.jpg)](https://www.youtube.com/watch?v=I4V-yftdFvY&list=PLAdApLDg0hIrAjCF1QkuuKAXbpH37Sb4C&index=1)    
> _Screencast instalação_

[Veja a documentação completa na wiki.](https://bitbucket.org/fbiz/hyojun.couch-bootstrap/wiki/Home)

## Como contribuir

Participe das conversas no slack e na lista de issues: discuta, abra, investigue ou resolva issues. Existem diversas melhorias a serem feitas! :)

Antes de começar a produzir, converse e veja se não há outra pessoa trabalhando na mesma coisa. Fique por dentro dos milestones do projeto.

Faça um fork do projeto e [envie um pull-request](https://bitbucket.org/fbiz/hyojun.couch-bootstrap/pull-request/new) para o branch `working`.

Utilizamos [semver](http://semver.org/) para versionar o projeto e abrimos uma tag no formato "major.minor.fix" para cada release público.

Canal do slack: https://fbiz.slack.com/messages/hyojun/

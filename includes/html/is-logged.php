<cms:embed "admin/builder-admin-button.php" />
<cms:php>
	// <?
	global $CTX;

	$is_share = '0';

	switch ($_SERVER['HTTP_USER_AGENT']) {
		case 'Twitterbot':
		case 'Twitterbot/1.0':
		case 'Facebot':
		case 'facebookexternalhit/1.1':
		case 'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)':
			$is_share = '1';
			break;
	}

	$CTX->set('is_share', $is_share);
	// ?>
</cms:php>
<cms:if (is_share eq "0") && k_user_access_level lt "2" >
	<cms:if k_user_access_level lt "2">
		<cms:redirect k_login_link />
	</cms:if>
</cms:if>

<cms:embed "admin/share.php" />

<cms:if facebook_url == "">
	<cms:set facebook_url="<cms:show url_root />" />
</cms:if>

<cms:if facebook_use == "1" >
	<meta property="og:title" content="<cms:show facebook_title />" />
	<meta property="og:description" content="<cms:show facebook_description />" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="<cms:show facebook_image />" />
	<meta property="og:url" content="<cms:show facebook_url />" />
</cms:if>

<cms:if twitter_use == "1" >
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="<cms:show twitter_site />">
	<meta name="twitter:creator" content="<cms:show twitter_creator />">
	<meta name="twitter:title" content="<cms:show twitter_title />">
	<meta name="twitter:description" content="<cms:show twitter_description />">
	<meta name="twitter:image" content="<cms:show twitter_image />">
</cms:if>

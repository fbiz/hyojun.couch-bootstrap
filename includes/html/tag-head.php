<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<cms:embed "admin/seo.php" />

<title><cms:show page_title /> <cms:get_custom_field "site_title" masterpage="globals.php" /> </title>
<meta name="description" content="<cms:show page_description />">
<meta name="author" content="">

<meta name="robots" content="INDEX, FOLLOW">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<cms:embed "html/share.php" />

<link rel="stylesheet" type="text/css" href="<cms:show url_assets />css/structure.css">
<cms:each custom_css sep=",">
	<link rel="stylesheet" type="text/css" href="<cms:show url_assets />css/<cms:show item />">
</cms:each>

<cms:embed "html/no-touch.php" />
<cms:embed "html/no-js.php" />

<script src="<cms:show url_assets />js/dist/main-structure.js" async defer></script>
<cms:each custom_js sep=",">
	<script src="<cms:show url_assets />js/dist/<cms:show item />" async defer></script>
</cms:each>

<cms:php>
	global $CTX;
	$root = $CTX->get('k_site_link');
	$uri = 'studio/#/open/';
	$page = substr(str_replace($CTX->get('k_site_link'), '', $CTX->get('k_page_link')), 0, -1);
	$CTX->set('builder_link', $root . $uri . $page);
</cms:php>
<cms:editable name="builder_link"
	label=""
	type="message"
	order="-100"
	hidden="1"
>

<style>
	@font-face {
		font-family: "icomoon";
		src: url("<cms:show k_site_link />/studio/fonts/icomoon.eot?gdtl95");
		src: url("<cms:show k_site_link />/studio/fonts/icomoon.eot?gdtl95#iefix") format("embedded-opentype"),
			  url("<cms:show k_site_link />/studio/fonts/icomoon.ttf?gdtl95") format("truetype"),
			  url("<cms:show k_site_link />/studio/fonts/icomoon.woff?gdtl95") format("woff"),
			  url("<cms:show k_site_link />/studio/fonts/icomoon.svg?gdtl95#icomoon") format("svg");
		font-weight: normal;
		font-style: normal;
	}

	.builder-button,
	.builder-button:link,
	.builder-button:visited,
	.builder-button:active,
	.builder-button:hover {
		background-color: #000;
		border: 0;
		border-radius: 0;
		color: #fff;
		display: inline-block;
		font-size: 14px;
		font-weight: normal;
		height: 30px;
		line-height: 30px;
		padding: 0 8px;
		text-decoration: none;
		vertical-align: middle;
	}

	.builder-button:hover {
		background-color: #3d4249;
	}

	.builder-button .builder-icon {
		color: #fff;
		display: inline-block;
		font-size: 19px;
		font-weight: normal;
		height: 30px;
		line-height: 30px;
		margin-right: 4px;
		margin-top: 0;
		vertical-align: top;
	}

	.builder-button .builder-icon:before {
		content: "";
		display: inline-block;
		font-family: icomoon;
	}

	.builder-button .icon-admin:before {
		color: #8fc06a;
		content: "\e254";
	}
</style>

<a class="builder-button" href="<cms:show builder_link />" target="_blank">
	<span class="builder-icon icon-admin"></span>
	Editar os módulos da página (Studio).
</a>
</cms:editable>
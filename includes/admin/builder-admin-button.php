<cms:editable name="builder_global_link"
	label=""
	type="message"
	order="-100"
	hidden="1"
>
<script>
	document.addEventListener("DOMContentLoaded", function() {
		var menu = document.querySelector('.templates');
		var separator = document.querySelector('.template-separator').cloneNode(true);
		var li = document.createElement('li');
		li.className += 'template'
		li.innerHTML = document.getElementById('buiderButtonTemplate').innerHTML;

		menu.appendChild(separator);
		menu.appendChild(li);
	});
</script>
<style>
	#k_element_builder_global_link {
		display: none;
	}
</style>

<script type="text/html" id="buiderButtonTemplate">
<a href="<cms:show k_site_link />studio/" target="_blank">
	<span class="builder-icon icon-admin"></span>
	Criar uma página (Studio)
</a>
</script>
</cms:editable>

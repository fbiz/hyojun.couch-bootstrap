<cms:editable name="seo" label="SEO" type="group" />

<cms:editable name="admin_injection" type="message">
	<style>
		#k_element_admin_injection {
			display: none;
		}
	</style>
	<script type="text/javascript">
		window.addEvent('domready', function(){
			var holder = $$('#f_seo .group-slider')[0];
			$$('#k_page_name').inject(holder, 'top');
			$$('#k_page_title').inject(holder, 'top');
			$$('#k_page_title b')[0].innerHTML = 'Título da página:';
			$$('#k_page_name b')[0].innerHTML = '"Slug" (nome que aparece no endereço):';
		});
	</script>
</cms:editable>

<cms:if k_is_home != "1" && k_is_page == "0">
	<cms:editable name="page_title"
		label="Título da página"
		desc="<cms:show field_title_desc />"
		type="text"
		hidden="1"
		group="seo"
		order="0"><cms:show fallback_page_title /></cms:editable>
</cms:if>
<cms:if page_title == "">
	<cms:set page_title="<cms:show k_page_title />" />
</cms:if>
<cms:if page_title != "">
	<cms:set page_title="<cms:show page_title /> - " />
</cms:if>

<cms:editable name="page_description"
	label="Descrição da página"
	desc="<cms:show field_description_desc />"
	type="text"
	hidden="1"
	group="seo"
	order="0"><cms:show fallback_page_description /></cms:editable>
<cms:if page_description == "">
	<cms:set page_description="<cms:get_custom_field 'site_description' masterpage='globals.php' />" />
</cms:if>

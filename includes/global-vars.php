<cms:set url_root="<cms:show k_site_link />" />
<cms:set url_assets="<cms:show k_site_link />assets/" />

<cms:php>
	// <?
	global $var_list;
	$var_list = array();
	// ?>
</cms:php>

<cms:templates order='asc'>
	<cms:if k_template_is_executable>
		<cms:php>
		// <?
			global $CTX;
			global $var_list;

			$name = 'url_' . $CTX->get('k_template_name');
			$name = preg_replace('/\\.(php|html)/', '', $name);
			$name = preg_replace('/\\//', '_', $name);
			$var_list[] = array($name, $CTX->get('k_template_link'));
		// ?>
		</cms:php>
	</cms:if>
</cms:templates>

<cms:php>
// <?
	global $CTX;
	global $var_list;

	foreach ($var_list as $item) {
		$CTX->set($item[0], $item[1]);
	}
// ?>
</cms:php>

<cms:set rt_default="bold,italic | link,unlink |
	source,showblocks,removeformat" />
<cms:set rt_height="150" />

<cms:set field_title_desc='Ideal entre entre 10 e 70 caracteres —
	espaços incluídos. A tag &lt;title&gt; é o título mostrado em uma
	busca no Google.' />
<cms:set field_description_desc='Ideal entre 70 e 160 caracteres —
	espaços incluídos. A tag &lt;meta type="description"&gt; é o texto
	que aparece logo abaixo do título em uma busca no Google.' />

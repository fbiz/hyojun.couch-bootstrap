module.exports = function(grunt) {

	'use strict';

	var path = require('path');
	var couch = require('./modules/couch');
	var imageminConfig = require('./modules/imagemin-config');

	// "PRIVATE" TASKS (escondendo do help)
	if (!grunt.option('help')) {

		grunt.registerTask('couch-install',
			[
				'Acessa o site com os dados padrão de usuário e senha. Opções possíveis:',
				'* --user - nome do usuário (padrão "' + couch.data.config('fetch.user') + '")',
				'* --password - senha do usuário (padrão "' + couch.data.config('fetch.password') + '")',
				'* --url - url do servidor (padrão ' + couch.data.config('fetch.url') + ').'
			].join('\n'),
			function() {
				var done = this.async();
				couch.connections.install()
					.then(function() {
						grunt.log.ok([
							'Couch instalado com sucesso. Acesse ' + couch.data.url() + 'couch/adm.php ,',
							'entre com o usuário "' + couch.data.user() + '" e senha "' + couch.data.password() + '"'
						].join(' '));
						done();
					})
					.catch(done);
			});

		grunt.registerTask('fetch-bkp',
			'Executa a task fetch com opções --dest=bkp e --assets-only',
			function() {
				if (grunt.file.exists(couch.data.bkpPath())) {
					grunt.file.delete(couch.data.bkpPath());
				}
				grunt.option('dest', couch.data.bkpPath());
				grunt.option('assets-only', true);
				grunt.option('override-assets', true);
				grunt.task.run('copy:assets-backup');
				grunt.task.run('fetch');
			});

		grunt.registerTask('generate-backup-zip',
			'Gera o zip da pasta temporária "' + couch.data.bkpPath() + '" no formato couch-bkp_*.zip',
			function() {
				var done = this.async();

				couch.backup.generate()
					.then(function() {
						if (grunt.file.exists(couch.data.bkpPath())) {
							grunt.file.delete(couch.data.bkpPath());
						}
						done();
					})
					.catch(done);
			});

		grunt.registerTask('restore-zip',
			'Restaura o ZIP no formato couch-bkp_*.zip',
			function() {
				var done = this.async();
				var file = grunt.option('file');

				if (!file) {
					file = grunt.file.expand({}, [
						"couch-bkp_*.zip"
					]);
				}

				couch.backup.restore(file)
					.then(done, done);
			});

		grunt.registerTask('clear-restore',
			'Apaga os arquivos gerados pelo restore.',
			function() {
				var dump = 'couch' + path.sep + 'install-ex.php';
				if (grunt.file.exists(dump)) {
					grunt.file.delete(dump);
				}
			});

		grunt.registerTask('clear-static',
			'Limpa qualquer versão anterior de um site gerado.',
			function() {
				if (grunt.file.exists(couch.data.dest())) {
					grunt.file.delete(couch.data.dest());
				}
			});

		grunt.registerTask('apply-version',
			'Aplica versão nos HTML e CSS',
			function() {
				var files = grunt.file.expand({}, [
					path.join(couch.data.dest() + '**/*.{html,css}')
				]);

				files.forEach(function(val) {
					var data = couch.modifiers.applyVersion({
						body: grunt.file.read(val)
					});
					grunt.file.write(val, data.body);
				});
			});

		grunt.registerTask('go-static-imagemin',
			'Busca pelas imagens do projeto, monta o config e roda imagemin. Útil para o go-static.',
			function() {
				grunt.config('imagemin', imageminConfig.generate());
				grunt.task.run('imagemin');
			});

		grunt.registerTask('do-rename-route',
			[
				'Renomeia uma "rota" do couchcms, movendo fisicamente os arquivos,',
				'alterando o banco de dados e todas as referências no projeto. Opções possíveis:',
				'* --from - caminho atual (do arquivo php)',
				'* --to - nome a ser modificado no formato "diretorio/sub/arquivo.php"'
			].join(' '),
			function() {
				couch.route.rename(grunt.option('from'), grunt.option('to'));
			});
	}

	grunt.config('get-dump.root', '.');
	grunt.registerTask('get-dump',
		'Baixa o dump do banco de dados em couch/install-ex.php.',
		function() {
			var done = this.async();
			couch.connections.getDump()
				.then(function(res) {
					var dest = path.join(grunt.config('get-dump.root'), 'couch', 'install-ex.php');
					grunt.file.write(dest, res.body);
					done();
				})
				.catch(done);
		});

	grunt.registerTask('rename-route',
		[
			'Renomeia uma "rota" do couchcms, movendo fisicamente os arquivos,',
			'alterando o banco de dados e todas as referências no projeto'
		].join(' '),
		function() {
			grunt.task.run('get-dump');
			grunt.task.run('do-rename-route');
			grunt.task.run('reset');
		});

	grunt.registerTask('go-static',
		[
			'Gera os arquivos estáticos do site e copia para a pasta ' + couch.data.dest() + '.',
			'* --url-prod - url que será utilizada nos htmls estáticos (padrão "' + couch.data.config('static.urlProd') + '").',
			'* --url-meta - url absoluta para ser utilizada em metatags e no sitemap (padrão valor de url-prod).',
			'* --dest - diretório de destino onde o site estático será gerado (padrão "' + couch.data.config('static.dest') + '").',
			'* --extension - extensão padrão para páginas de saída (padrão "' + couch.data.config('static.extension') + '");',
			'* --avoid-default - não roda o default task antes do go-static (padrão false);',
			'* --quality-small - nível de compressão de imagens com menos de 450*450px de area (padrão ' + couch.data.config('static.qualitySmall') + ');',
			'* --quality-large - nível de compressão de imagens com mais de 450*450px de area (padrão ' + couch.data.config('static.qualityLarge') + ');',
			'* extende todas as opções da task "fetch";'
		].join('\n'),
		function() {
			if (!grunt.option('avoid-default')) {
				grunt.task.run('default');
			}
			grunt.task.run('clear-static');
			grunt.task.run('fetch');
			grunt.task.run('copy:assets');
			grunt.task.run('apply-version');
			grunt.task.run('go-static-imagemin');
			grunt.task.run('htmlmin:static');
		});

	grunt.registerTask('build',
		[
			'Força a atualização do admin e banco de dados acessando as páginas (php) necessárias.',
			'* --user - nome do usuário no couch (padrão "' + couch.data.config('fetch.user') + '")',
			'* --password - senha do usuário no couch (padrão "' + couch.data.config('fetch.password') + '")',
			'* --url - url do servidor com o couch que será acessado (padrão ' + couch.data.config('fetch.url') + ').'
		].join('\n'),
		function(file) {
			var done = this.async();
			file = file || grunt.config('build.file');

			couch.build.build(file).then(done, done);
		});

	grunt.registerTask('restore',
		[
			'Faz a restauração baseado no arquivo ZIP gerado pela task "backup".',
			'* --file - caminho do arquivo relativo à posição do gruntfile;',
			'* --no-backup - ignora o passo do backup (padrão false);'
		].join('\n'),
		function() {
			if (grunt.option('no-backup') !== true) {
				grunt.task.run('backup');
			}
			grunt.task.run('restore-zip');
			grunt.task.run('exec:vm-mysqldrop');
			grunt.task.run('exec:vm-mysql');
			grunt.task.run('sync');
			grunt.task.run('couch-install');
			grunt.task.run('build');
			grunt.task.run('clear-restore');
		});

	grunt.registerTask('backup',
		'Gera o ZIP de backup com o dump do banco de dados e imagens usadas no site.',
		function() {
			grunt.config('get-dump.root', couch.data.bkpPath());
			grunt.task.run('fetch-bkp');
			grunt.task.run('get-dump');
			grunt.task.run('generate-backup-zip');
		});

	grunt.registerTask('update-modified-date',
		'Atualiza a data de last-modified dos arquivos de acordo com repositório do GIT',
		function() {
			require('./modules/couch-modified-date').syncModifiedDate();
		});

	grunt.registerTask('fetch',
		'Atualiza os last-modified e baixa os arquivos do site',
		['update-modified-date', 'fetch-files']);

	grunt.registerTask('fetch-files',
		[
			'Baixa a versão HTML do site + imagens.',
			'* --url - url do servidor com o couch que será acessado (padrão ' + couch.data.config('fetch.url') + ').',
			'* --user - nome do usuário no couch (padrão "' + couch.data.config('fetch.user') + '")',
			'* --password - senha do usuário no couch (padrão "' + couch.data.config('fetch.password') + '")',
			'* --override-assets - sobrepõe as imagens locais com as versões atualizadas do servidor (padrão false).',
			'* --assets-only - não gera os arquivos .html (padrão false);',
			'* --connection-limit - limite de conexões simultâneas (padrão ' + couch.data.config('fetch.connectionLimits') + ');',
			['* --before-parse - módulo js, relativo ao gruntfile.js, que será executado antes de qualquer',
			'replace nos arquivos de saída. Recebe um objeto {page: "endereço-da-página", body: "conteúdo da página"}',
			'e deve retornar o valor alterado;'].join(' ')
		].join('\n'),
		function() {
			var done = this.async();
			var mod = couch.modifiers;
			var fetch = couch.fetch.request()
				.pipe(mod.map(mod.beforeParsePage))
				.pipe(mod.map(mod.convertFileNameInPage))
				.pipe(mod.map(mod.updateToProdUrl))
				.pipe(mod.map(mod.updateMetaTagsUrl))

			fetch.download().then(done, done);
		});

	grunt.config('copy.assets', {
		cwd: 'assets/',
		src: [
			'**/*',
			'!image/**/*.{png,jpg,jpeg,gif}',
			'!sass/**/*',
			'!js/**/*',
			'js/dist/**/*',
		],
		dest: path.join(couch.data.dest(), 'assets'),
		expand: true
	});

	grunt.config('copy.assets-backup', {
		cwd: 'assets/',
		src: [
			'**/*',
			'!sass/**/*',
			'!js/**/*',
			'js/dist/**/*'
		],
		dest: path.join(couch.data.bkpPath(), 'assets'),
		expand: true
	});

	grunt.config('imagemin', imageminConfig.generate());

	grunt.config('htmlmin.static', {
		options: {
			caseSensitive: true,
			collapseBooleanAttributes: false,
			collapseWhitespace: true,
			conservativeCollapse: true,
			keepClosingSlash: true,
			lint: false,
			maxLineLength: 120,
			minifyCSS: true,
			minifyJS: true,
			minifyURLs: false,
			preserveLineBreaks: false,
			preventAttributesEscaping: false,
			removeAttributeQuotes: false,
			removeCDATASectionsFromCDATA: false,
			removeComments: true,
			removeCommentsFromCDATA: true,
			removeEmptyAttributes: false,
			removeEmptyElements: false,
			removeIgnored: false,
			removeOptionalTags: false,
			removeRedundantAttributes: false,
			removeScriptTypeAttributes: false,
			removeStyleLinkTypeAttributes: false,
			useShortDoctype: false
		},
		cwd: couch.data.dest(),
		src: [
			'**/*' + couch.data.extension(),
			'**/*.xml'
		],
		dest: couch.data.dest(),
		expand: true
	});

	grunt.event.on("watch", function(action, filepath, target) {
		if (target === "build") {
			grunt.config("build.file", filepath);
		}
	});
};

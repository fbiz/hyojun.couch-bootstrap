module.exports = function(grunt) {
	grunt.config.set('watch', {
		build: {
			files: [
				'**/*.php',
				'!couch/**/*.php'
			],
			tasks: ['sync', 'build', 'exec:touch'],
			options: {
				livereload: true,
				spawn: false
			}
		},
		requirejs_partial: {
			files: [
				'assets/**/*.js',
				'!assets/js/dist/**/*.js'
			],
			tasks: ['requirejs_partial', 'sync', 'exec:touch'],
			options: {
				livereload: true,
				spawn: false
			}
		},
		sass: {
			files: ['assets/sass/**/*.scss'],
			tasks: ['css:dev', 'sync', 'exec:touch']
		},
		noop: {
			options: {
				livereload: true
			},
			files: 'noop.config'
		}
	});
};

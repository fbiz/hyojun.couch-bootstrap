module.exports = function(grunt) {

	'use strict';

	var couch = require('./modules/couch');
	var connectionStringPath = 'couch/config.php';
	var tcDeployConfigPath = 'couch/addons/tc-deploy/tc-deploy-config.json';
	var ssh = function(){
		return [
			'ssh',
			'-p ' + couch.data.sshPort(),
			'-t',
			'-o Compression=yes',
			'-o DSAAuthentication=yes',
			'-o LogLevel=FATAL',
			'-o StrictHostKeyChecking=no',
			'-o UserKnownHostsFile=/dev/null',
			'-o IdentitiesOnly=yes',
			'-i ' + couch.data.sshKey(),
			couch.data.sshUser() + '@' + couch.data.sshUrl()
		];
	};

	grunt.config.set('exec', {
		'npm': 'npm install',
		'bower': 'bower install -F',
		'bundle': 'bundle install',
		'touch': 'touch noop.config',
		'vm-start': 'vagrant up',
		'vm-stop': 'vagrant halt',
		'vm-restart-apache': {
			cmd: function() {
				return ssh().concat('"sudo apachectl restart"').join(' ');
			}
		},
		'vm-fix-perm': {
			cmd: function() {
				return ssh().concat([
					'"sudo chgrp -R www-data ' + couch.data.sshRemotePath() + ' && ',
					'sudo chmod -R 777 ' + couch.data.sshRemotePath() + '"'
				]).join(' ');
			}
		},
		'vm-mysql': {
			cmd: function() {
				return ssh().concat([
					'"mysql -u' + couch.data.dbUser() + ' --password=' + couch.data.dbPassword(),
					'-e \'create database if not exists ' + couch.data.dbName() + '\'"'
				]).join(' ');
			}
		},
		'vm-mysqldrop': {
			cmd: function() {
				return ssh().concat([
					'"mysql -u' + couch.data.dbUser() + ' --password=' + couch.data.dbPassword(),
					'-e \'drop database if exists ' + couch.data.dbName() + '\'"'
				]).join(' ');
			}
		}
	});

	var sshSync = function() {
		return [
			'rsync',
			'-rvuzWc',
			'--exclude-from=.rsync-ignore',
			'-e "'
		]
		.concat(ssh().slice(0, -1))
		.concat([
			'"',
			'.',
			couch.data.sshUser() + '@' + couch.data.sshUrl() + ':' + couch.data.sshRemotePath()
		]).join(' ');
	};

	grunt.config('exec.sync', {
		cmd: function() {
			return sshSync();
		}
	});

	grunt.config('exec.sync-all', {
		cmd: function() {
			return sshSync().replace('-rvuzW', '-rvzW');
		}
	});

	grunt.config('exec.sync-upload', {
		cmd: function() {
			return sshSync()
				.replace('.rsync-ignore ', '.rsync-ignore-upload ')
				.replace(' . ', ' assets ')
				.replace(couch.data.sshRemotePath(), couch.data.sshRemotePath());
		}
	});

	grunt.config.set('watch.sync',{
		files: [
			'assets/css/**/*.css',
			'**/*.config',
			'assets/js/dist/**/*.js',
			'assets/img/**/*.*',
			'assets/fonts/**/*.*',
			'Views/**/*.*',
			'App_Code/**/*.*',
			'!noop.config'
		],
		tasks: ['sync', 'exec:touch']
	});

	grunt.registerTask('sync',
		[
			'Sincroniza apenas os arquivos modificados para a máquina virtual.',
			'* --ssh-key - local do arquivo com a chave privada (padrao .vagrant/.../privatekey)',
			'* --ssh-url - URL do SSH (padrao "' + couch.data.config('ssh.url') + '")',
			'* --ssh-port - porta do SSH (padrao "' + couch.data.config('ssh.port') + '")',
			'* --ssh-user - Usuário do SSH (padrao "' + couch.data.config('ssh.user') + '")',
			'* --ssh-remote-path - Caminho remoto no SSH (padrao "' + couch.data.config('ssh.remotePath') + '")'
		].join('\n'),
		['exec:sync', 'exec:sync-upload', 'exec:vm-fix-perm']);

	grunt.registerTask('sync:all',
		'Envia todos os arquivos do projeto para a máquina virtual;',
		['exec:sync-all', 'exec:vm-fix-perm']);

	grunt.registerTask('update-config',
		[
			'Atualiza os dados do site no config.php.',
			'* --url - variável K_SITE_URL (padrão "' + couch.data.config('fetch.url') + '")',
			'* --db-user - variável K_DB_USER (padrão "' + couch.data.config('db.user') + '")',
			'* --db-password - variável K_DB_PASSWORD (padrão "' + couch.data.config('db.password') + '")',
			'* --db-host - variável K_DB_HOST (padrão "' + couch.data.config('db.host') + '")',
			'* --db-name - variável K_DB_NAME (padrão "' + couch.data.config('db.name') + '")'
		].join('\n'),
		function() {
			grunt.file.preserveBOM = true;
			var content = grunt.file.read(connectionStringPath)
				.replace(/('K_SITE_URL', ')(.*?)'/, '$1' + couch.data.url() + '\'')
				.replace(/('K_DB_NAME', ')(.*?)'/, '$1' + couch.data.dbName() + '\'')
				.replace(/('K_DB_USER', ')(.*?)'/, '$1' + couch.data.dbUser() + '\'')
				.replace(/('K_DB_PASSWORD', ')(.*?)'/, '$1' + couch.data.dbPassword() + '\'')
				.replace(/('K_DB_HOST', ')(.*?)'/, '$1' + couch.data.dbHost() + '\'');
			grunt.file.write(connectionStringPath, content);
		});

	grunt.registerTask('vm-start',
		'Atualiza os arquivos de config com as portas corretas e inicia a VM',
		['exec:vm-start', 'update-config', 'sync']);

	grunt.registerTask('vm-stop',
		'Shortcut para desligar a VM',
		['exec:vm-stop']);

	grunt.registerTask('update-tc-deploy-config',
		[
			'Atualiza os dados do site para publicação via TeamCity.',
			'* --tc-api-url - endereço da RESTAPI do TeamCity;',
			'* --tc-build-id - ID da task do TeamCity que faz o publish;',
			'* --tc-project-id - ID do projeto no TeamCity;',
			'* --tc-auth - Base64 do basic authentication do TeamCity;',
			'* --tc-confirm - Mensagem de confirmação para publicação no TeamCity;'
		].join('\n'),
		function() {
			grunt.file.preserveBOM = true;
			var content = grunt.file.read(tcDeployConfigPath)
				.replace(/("api-root":.*?")(.*?)(")/g, '$1' + (couch.data.tcApiUrl() || '$2') + '$3')
				.replace(/("build-id":.*?")(.*?)(")/g, '$1' + (couch.data.tcBuildId() || '$2') + '$3')
				.replace(/("project-id":.*?")(.*?)(")/g, '$1' + (couch.data.tcProjectId() || '$2') + '$3')
				.replace(/("auth":.*?")(.*?)(")/g, '$1' + (couch.data.tcAuth() || '$2') + '$3')
				.replace(/("confirm-message":.*?")(.*?)(")/g, '$1' + (couch.data.tcConfirm() || '$2') + '$3');
			grunt.file.write(tcDeployConfigPath, content);
		});

	grunt.registerTask('packages',
		'Instala os pacotes necessários para rodar o projeto;',
		['exec:npm', 'exec:bower', 'exec:bundle']);

	grunt.registerTask('reset',
		'Apaga o banco de dados e a instalação do CouchCMS na máquina virtual e reinstala.',
		['exec:vm-mysqldrop', 'setup']);
};

var tap = require('tape');

tap.test('-- COUCH RENAME ROUTES --', function(t) {
	var couchRoute = require('../modules/couch-route.js');

	t.test('Pipeline - objeto alterado é passado para o próximo pipe', function(t) {
		var data = {page: 'teste', body: 'teste de conteúdo' };
		var pipeline = new couchRoute.Pipeline(data);
		var changedData = {};
		var changedData2 = {};

		pipeline
			.pipe(function(data) {
				data.body = 'modified';
				return data;
			})
			.pipe(function(data) {
				changedData = data;
				return {
					body: data.body,
					page: 'outro-teste'
				};
			})
			.pipe(function(data) {
				changedData2 = data;
				return data;
			});

		t.deepEqual(changedData, {page: 'teste', body: 'modified'},
			'atributo "body" é alterado no pipe e passado pra frente');

		t.deepEqual(changedData2, {page: 'outro-teste', body: 'modified'},
			'atributo "page" é alterado no pipe e passado pra frente');

		t.end();
	});

	t.test('Alteração da mudança de rotas dentro dos conteúdos de arquivos', function(t) {
		t.test('- dentro do atributo "masterpage"', function(t) {
			var data = {
				from: 'from/page.php',
				to: 'to/page.php',
				body: '<cms:pages masterpage="from/page.php"></cms:pages>'
			};
			var expectedData = '<cms:pages masterpage="to/page.php"></cms:pages>';

			t.isEqual(couchRoute.replaceContent(data).body, expectedData,
				'from/page.php deve mudar para to/page.php');
			t.end();
		});

		t.test('- variáveis "url_*" geradas dinamicamente', function(t) {
			var data = {
				from: 'from/page.php',
				to: 'to/page.php',
				body: '<kshow url_from_page />'
			};
			var expectedData = '<kshow url_to_page />';

			t.isEqual(couchRoute.replaceContent(data).body, expectedData,
				'url_from_page deve mudar para url_to_page');
			t.end();
		});

		t.test('- não encontra nada (e não escreve o arquivo)', function(t) {
			var data = {
				from: 'from/page.php',
				to: 'to/page.php',
				body: '<cms:pages masterpage="other/page.php"></cms:pages>'
			};

			t.notOk(couchRoute.replaceContent(data).write, 'from/page.php não é encontrado e mantém arquivo intacto');
			t.end();
		});
	});

	t.test('Alteração da mudança de rotas dentro do conteúdo do dump', function(t) {
		var data = {
			from: 'from/page.php',
			to: 'to/page.php',
			body: '$k_stmt_pre."(6, \'from/page.php\', \'\', 1, 1, NULL, 0, 0, 0, 25, 0, 0, 0, NULL, NULL);";'
		};
		var expectedData = '$k_stmt_pre."(6, \'to/page.php\', \'\', 1, 1, NULL, 0, 0, 0, 25, 0, 0, 0, NULL, NULL);";';

		t.isEqual(couchRoute.replaceDumpContent(data).body, expectedData,
			'from/page.php deve mudar para to/page.php');
		t.end();
	});

	t.test('Alteração da mudança de rotas dentro de repeatables (base64) no dump', function(t) {

		t.test('- encontra e modifica', function(t) {
			var data = {
				from: 'from/page.php',
				to: 'to/page.php',
				body: "'<cms:editable\\r\\nname=\\'rdrop\\'\\r\\ntype=\\'__repeatable\\'\\r\\nhidden=\\'1\\'\\r\\nschema=\\'a:2:{i:0;a:41:{s:4:\\\"name\\\";s:10:\\\"opt_values\\\";s:32:\\\"dmFsb3IgMSB8IGZyb20vcGFnZS5waHA=\\\";s:5:\\\"_html\\\";s:112:\\\"PGNtczplZGl0YWJsZQ0KbmFtZT0nZHJvcCcNCnR5cGU9J2Ryb3Bkb3duJw0Kb3B0X3ZhbHVlcz0ndmFsb3IgMSB8IGZyb20vcGFnZS5waHAnLz4=\\\";}\\'/>"
			};
			var expectedData = "'<cms:editable\\r\\nname=\\'rdrop\\'\\r\\ntype=\\'__repeatable\\'\\r\\nhidden=\\'1\\'\\r\\nschema=\\'a:2:{i:0;a:41:{s:4:\\\"name\\\";s:10:\\\"opt_values\\\";s:28:\\\"dmFsb3IgMSB8IHRvL3BhZ2UucGhw\\\";s:5:\\\"_html\\\";s:108:\\\"PGNtczplZGl0YWJsZQ0KbmFtZT0nZHJvcCcNCnR5cGU9J2Ryb3Bkb3duJw0Kb3B0X3ZhbHVlcz0ndmFsb3IgMSB8IHRvL3BhZ2UucGhwJy8+\\\";}\\'/>";

			t.isEqual(couchRoute.replaceDumpBase64(couchRoute.regExpDumpBase64)(data).body, expectedData,
				'from/page.php deve mudar para to/page.php dentro do base64 e mudar o lengh');
			t.ok(couchRoute.replaceDumpBase64(couchRoute.regExpDumpBase64)(data).write,
				'como from/page.php muda para to/page.php, retorna a flag verdadeira para escrever o arquivo.');
			t.end();
		});

		t.test('- não encontra e mantém arquivo intacto', function(t) {
			var data = {
				from: 'from/page.php',
				to: 'to/page.php',
				body: "'<cms:editable\\r\\nname=\\'rdrop\\'\\r\\ntype=\\'__repeatable\\'\\r\\nhidden=\\'1\\'\\r\\nschema=\\'a:2:{i:0;a:41:{s:4:\\\"name\\\";s:10:\\\"opt_values\\\";s:32:\\\"dmFsb3IgMSB8IG90aGVyL3BhZ2UucGhw\\\";s:5:\\\"_html\\\";s:112:\\\"PGNtczplZGl0YWJsZQ0KbmFtZT0nZHJvcCcNCnR5cGU9J2Ryb3Bkb3duJw0Kb3B0X3ZhbHVlcz0ndmFsb3IgMSB8IG90aGVyL3BhZ2UucGhwJy8+\\\";}\\'/>"
			};

			t.notOk(couchRoute.replaceDumpBase64(couchRoute.regExpDumpBase64)(data).write,
				'from/page.php não é encontrado e mantém o arquivo intacto');
			t.end();
		});
	});
});

var root = 'grunt/test';
var fs = require('fs');
var api = {};

api.mockUpConvertName = function() {
	var mock = {
		couch: {
			data: require('../../modules/couch-data').mock({
				config: {
					"fetch": {
						"port": "1144",
						"url": "http://localhost:1144/",
						"user": "system",
						"password": "admin",
						"connectionLimits": 5
					},
					"static": {
						"urlProd": "./",
						"urlMeta": "http://localhost:1144/",
						"dest": "_site",
						"extension": ".html",
						"convert": {
							"contato/": "contact/index.html",
							"atracoes/": "attractions/sub/index.html",
							"/^produtos\\/(.*)\\.html/": "products/$1.html"
						},
						"download": {
							"ext": [
								"jpe?g",
								"json"
							]
						}
					}
				}
			})
		}
	};
	return require('../../modules/couch-fetch').mock(mock);
};

api.mockUpQueueAssets = api.mockUpConvertName;

api.mockUpCompareDates = function(opt) {
	opt = opt || {};
	var mock = {
		grunt: {
			file: {
				exists: function() {
					return false;
				}
			}
		},
		fs: {
			statSync: function() {
				return {
					mtime: opt.localDate
				}
			}
		}
	};

	return require('../../modules/couch-fetch').mock(mock);
};

api.mockUpQueueData = function(val) {
	return {
		body: fs.readFileSync(root + '/mock-data/' + val).toString(),
		page: 'http://localhost:1144/'
	};
};

module.exports = api;

var root = 'grunt/test';
var fs = require('fs');

module.exports = {
	getMockUpConfig: function(config) {
		var data = require('../../modules/couch-data').mock({
			config: {
				"fetch": {
					"port": "1144",
					"url": "http://localhost:1144/",
					"user": "system",
					"password": "admin",
					"connectionLimits": 5
				},
				"static": {
					"urlProd": "./",
					"urlMeta": "http://localhost:1144/",
					"dest": "_site",
					"extension": ".html",
					"forceMetaUrl": [
						"sitemap.xml",
						"/teste\\.(html|xml)/"
					],
					"version": {
						"replace": "_hc-ver_",
						"ignoreTags": ["script", "div"],
						"ext": [
							"css",
							"js",
							"jpe?g"
						]
					},
					"convert": {
						"contato/": "contact/index.html",
						"atracoes/": "attractions/sub/index.html",
						"/produtos\\/(.*?)\\.html/": "$1/produtos.html"
					}
				}
			}
		});

		config = config || {};

		if (config.version) {
			data.version = function() {
				return '1';
			};
		}

		data.port = function() {
			return '1144';
		};

		if (config.relative) {
			data.urlProd = function() {
				return './';
			};
		}

		if (config.absolute) {
			data.urlProd = function() {
				return 'http://www.site.com.br/';
			};
		}

		if (config.urlMeta) {
			data.urlMeta = function() {
				return 'http://www.site.com.br/';
			};
		}

		return require('../../modules/couch-modifiers').mock({
			couch: {
				data: data
			}
		});
	},

	getMockUpData: function(val) {
		return {
			body: fs.readFileSync(root + '/mock-data/' + val).toString()
		};
	}
};

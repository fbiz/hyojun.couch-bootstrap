var root = 'grunt/test';
var fs = require('fs');
var api = {};

api.getDataFromBash = function(data) {
	data = data || {};
	var mock = {
		grunt: {
			file: {
				exists: function() {
					return true;
				},
				read: function() {
					return 'b7560a66-b84b-4c8f-8fcd-6d34535cc6fb';
				},
				readJSON: function() {
					return {};
				}
			},
			option: function() {
				return null;
			}
		},
		config: {
			"fetch": {
				"port": "1144",
				"url": "http://localhost:1144/"
			}
		},
		childProccess: {
			execSync: function(value) {
				if (data.withError) {
					throw new Error('Some error');
				}
				switch (value) {
					case 'git remote -v 2> /dev/null':
						return [
							'origin	git@bitbucket.org:fbiz/hyojun.couch-bootstrap.git (fetch)',
							'origin	git@bitbucket.org:fbiz/hyojun.couch-bootstrap.git (push)'
						].join('\n');
						break;
					case 'VboxManage showvminfo b7560a66-b84b-4c8f-8fcd-6d34535cc6fb':
						return [
							'NIC 1 Rule(0):   name = http, protocol = tcp, host ip = , host port = 3333, guest ip = , guest port = 80',
							'NIC 1 Rule(1):   name = ssh, protocol = tcp, host ip = , host port = 4444, guest ip = , guest port = 22'
						].join('\n');
						break;
				}
			}
		}
	};
	return require('../../modules/couch-data').mock(mock);
}

api.getDataFromConfig = function() {
	var path = {
		sep: '/',
		normalize: function(value) {
			return value;
		}
	};
	var grunt = require('grunt');
	grunt.option = function() {
		return null;
	}
	grunt.file.exists = function(val) {
		return val === 'couch.config.json';
	};
	grunt.file.readJSON = function() {
		return {
			"fetch": {
				"port": "1144",
				"url": "http://localhost:1144/",
				"user": "system",
				"password": "admin",
				"connectionLimits": 5
			},
			"db": {
				"user": "root",
				"password": "159753",
				"host": "localhost",
				"name": "hyojun_couch_bootstrap"
			},
			"ssh": {
				"keyPath": ".vagrant/machines/default/virtualbox/private_key",
				"url": "127.0.0.1",
				"port": "2222",
				"user": "vagrant",
				"remotePath": "/var/www/html"
			},
			"static": {
				"urlProd": "./",
				"urlMeta": "http://localhost:1144/",
				"dest": "_site",
				"extension": ".html",
				"convert": {
					"sitemap/": "sitemap.xml"
				},
				"forceMetaUrl": [
					"sitemap.xml"
				],
				"ignore": [
					"/^globals/",
					"/^page-list/"
				],
				"download": {
					"ext": ["jpe?g", "png", "gif", "webp", "svg", "pdf", "zip"]
				},
				"version": {
					"replace": "_hc-ver_",
					"ext": ["css", "js", "ttf", "woff2?", "otf", "eot", "svg", "jpe?g", "png", "gif", "webp"]
				},
				"qualitySmall": 89,
				"qualityLarge": 75
			}
		};
	};

	var mock = {
		grunt: grunt,
		path: path
	};
	return require('../../modules/couch-data').mock(mock);
};

module.exports = api;

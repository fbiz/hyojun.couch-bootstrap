/* jshint maxlen: false */

var tap = require('tape');
var helper = require('./helpers/couch-modifiers');

tap.test('-- COUCH MODIFIERS --', function(t) {
	t.test('Converte o nome do arquivo em conteúdo da página', function(t) {
		var couchModifiers = helper.getMockUpConfig();

		t.test('- dentro de tag', function(t) {
			var result1 = couchModifiers.convertFileNameInPage({
				body: '<a href="http://localhost:1144/contato/">http://localhost:1144/contato/</a>'
			});
			t.isEqual(
				result1.body,
				'<a href="http://localhost:1144/contact/index.html">http://localhost:1144/contact/index.html</a>',
				'replace com string (contact/index)');

			var result2 = couchModifiers.convertFileNameInPage({
				body: '<a href="http://localhost:1144/produtos/index.html">http://localhost:1144/produtos/index2.html</a>'
			});
			t.isEqual(
				result2.body,
				'<a href="http://localhost:1144/index/produtos.html">http://localhost:1144/index2/produtos.html</a>',
				'invertendo caminho com regexp (produtos/index)');

			t.end();
		});

		t.test('- links lado-a-lado', function(t) {
			var result1 = couchModifiers.convertFileNameInPage({
				body: '"http://localhost:1144/contato/ http://localhost:1144/contato/'
			});
			t.isEqual(
				result1.body,
				'"http://localhost:1144/contact/index.html http://localhost:1144/contact/index.html',
				'replace com string (contact/index)');

			var result2 = couchModifiers.convertFileNameInPage({
				body: '"http://localhost:1144/produtos/index.html|http://localhost:1144/produtos/sub/index3.html'
			});
			t.isEqual(
				result2.body,
				'"http://localhost:1144/index/produtos.html|http://localhost:1144/sub/index3/produtos.html',
				'invertendo caminho com regexp (produtos/index)');

			t.end();
		});
	});

	t.test('Converte o caminho absoluto para relativo', function(t) {
		var couchModifiers = helper.getMockUpConfig({
			relative: true
		});

		t.test('- apenas alteração no root', function(t) {
			var data1 = {
				body: 'http://localhost:1144/produtos/index.html|http://localhost:1144/produtos/index.html"',
				page: 'http://localhost:1144/'
			};
			t.isEqual(
				couchModifiers.updateToProdUrl(data1).body,
				'./produtos/index.html|./produtos/index.html"',
				'Converte url para caminho relativo considerando que o escopo é root');

			var data2 = {
				body: 'http://localhost:1144/produtos/index.html|http://localhost:1144/produtos/index.html"',
				page: 'http://localhost:1144/produtos/'
			};
			t.isEqual(
				couchModifiers.updateToProdUrl(data2).body,
				'../produtos/index.html|../produtos/index.html"',
				'Converte url para caminho relativo considerando que o escopo é um sub-folder');

			var data3 = {
				body: 'http://localhost:1144/produtos/index.html|http://localhost:1144/produtos/index.html"',
				page: 'http://localhost:1144/link1/link2/link3/'
			};
			t.isEqual(
				couchModifiers.updateToProdUrl(data3).body,
				'../../../produtos/index.html|../../../produtos/index.html"',
				'Converte url para caminho relativo considerando que o escopo são 3 níveis abaixo do sub-folder');

			t.end();
		});

		t.test('- alteração no root em um caminho de arquivo alterado', function(t) {
			var data = {
				body: 'http://localhost:1144/product/index.html|http://localhost:1144/outro-folder/index.html"',
				page: 'http://localhost:1144/produtos/',
				redirect: 'http://localhost:1144/product/'
			};
			t.isEqual(
				couchModifiers.updateToProdUrl(data).body,
				'../product/index.html|../outro-folder/index.html"',
				'Converte url para caminho relativo considerando que o escopo é um sub-folder');

			t.end();
		});

		t.test('- config indica arquivo absoluto (ignora)', function(t) {
			var couchModifiers = helper.getMockUpConfig({
				absolute: true
			});
			var data = {
				body: 'http://localhost:1144/product/index.html|http://localhost:1144/outro-folder/index.html"',
				page: 'http://localhost:1144/produtos/'
			};
			t.isEqual(
				couchModifiers.updateToProdUrl(data).body,
				'http://www.site.com.br/product/index.html|http://www.site.com.br/outro-folder/index.html"',
				'Converte url para caminho relativo considerando que o escopo é um sub-folder');

			t.end();
		});
	});

	t.test('Inserção da versão de build', function(t) {
		var couchModifiers = helper.getMockUpConfig({version: true});

		t.test('- nas extensões definidas no config (dentro de aspas ou "url()")', function(t) {
			var result1 = couchModifiers.applyVersion({
				body: '"http://localhost:1144/product/index.js"|\'http://localhost:1144/product/index.css\''
			});
			t.isEqual(result1.body,
				'"http://localhost:1144/product/index.js?v=1"|\'http://localhost:1144/product/index.css?v=1\'',
				'Arquivo JS e CSS convertido dentro de aspas');

			var result2 = couchModifiers.applyVersion({
				body: [
					'url("http://localhost:1144/product/index.js")',
					'url(\'http://localhost:1144/product/index.css\')',
					'url(http://localhost:1144/product/index.css)'
				].join('\n')
			});
			t.isEqual(result2.body,
				[
					'url("http://localhost:1144/product/index.js?v=1")',
					'url(\'http://localhost:1144/product/index.css?v=1\')',
					'url(http://localhost:1144/product/index.css?v=1)'
				].join('\n'),
				'Arquivo JS e CSS convertido dentro de url()');

			t.end();
		});

		t.test('- nas extensões já com querystring definida', function(t) {
			var result1 = couchModifiers.applyVersion({
				body: '"http://localhost:1144/product/index.js?teste=1"|\'http://localhost:1144/product/index.css?t=1\''
			});
			t.isEqual(result1.body,
				'"http://localhost:1144/product/index.js?teste=1&v=1"|\'http://localhost:1144/product/index.css?t=1&v=1\'',
				'Arquivo JS e CSS com parâmetro extra');

			var result2 = couchModifiers.applyVersion({
				body: '"http://localhost:1144/product/index.js?teste=1&v=2"|\'http://localhost:1144/product/index.css?t=1&v=2\''
			});
			t.isEqual(result2.body,
				'"http://localhost:1144/product/index.js?teste=1&v=1"|\'http://localhost:1144/product/index.css?t=1&v=1\'',
				'Arquivo JS e CSS com parâmetro "v" que é substituído pelo novo version');

			t.end();
		});

		t.test('- substitui string livre _hc-ver_, definida no config', function(t) {
			var result = couchModifiers.applyVersion({
				body: '"http://localhost:1144/product/index.json?version=_hc-ver_"|_hc-ver_'
			});
			t.isEqual(result.body,
				'"http://localhost:1144/product/index.json?version=1"|1',
				'Arquivo js com substuição livre no parâmetro "version"');

			t.end();
		});

		t.test('- não insere o version dentro de tag script', function(t) {
			var result1 = couchModifiers.applyVersion({
				body: [
					'"http://localhost:1144/product/index.js"',
					'<script>"http://localhost:1144/product/index.js"</script>'
				].join('\n')
			});
			t.isEqual(result1.body,
				[
					'"http://localhost:1144/product/index.js?v=1"',
					'<script>"http://localhost:1144/product/index.js"</script>'
				].join('\n'),
				'Arquivo js apenas substituído fora da tag script');

			var result2 = couchModifiers.applyVersion({
				body: [
					'"http://localhost:1144/product/index.js"',
					'<script type="text/javascript">',
					'"http://localhost:1144/product/index.js"</script>',
					'"http://localhost:1144/product/index.js"',
					'<script>"http://localhost:1144/product/index.js"</script>',
					'"http://localhost:1144/product/index.js"'
				].join('\n')
			});
			t.isEqual(result2.body,
				[
					'"http://localhost:1144/product/index.js?v=1"',
					'<script type="text/javascript">',
					'"http://localhost:1144/product/index.js"</script>',
					'"http://localhost:1144/product/index.js?v=1"',
					'<script>"http://localhost:1144/product/index.js"</script>',
					'"http://localhost:1144/product/index.js?v=1"'
				].join('\n'),
				'Apenas substituído fora da tag script (valor igual a de dentro da tag)');

			var result3 = couchModifiers.applyVersion({
				body: '<script type="text/javascript" src="http://localhost:1144/product/index.js"></script>'
			});
			t.isEqual(result3.body,
				'<script type="text/javascript" src="http://localhost:1144/product/index.js?v=1"></script>',
				'Arquivo js deve ser substituído caso esteja como atributo da tag script');

			t.end();
		});

		t.test('- não insere o version dentro de tag customizada', function(t) {
			var result1 = couchModifiers.applyVersion({
				body: [
					'"http://localhost:1144/product/index.js"',
					'<div class="123">"http://localhost:1144/product/index.js"</div>'
				].join('\n')
			});
			t.isEqual(result1.body,
				[
					'"http://localhost:1144/product/index.js?v=1"',
					'<div class="123">"http://localhost:1144/product/index.js"</div>'
				].join('\n'),
				'Arquivo js apenas substituído fora da tag div COM classname (definida pelo config)');

			var result2 = couchModifiers.applyVersion({
				body: [
					'"http://localhost:1144/product/index.js"',
					'<div>',
					'"http://localhost:1144/product/index.js"</div>'
				].join('\n')
			});
			t.isEqual(result2.body,
				[
					'"http://localhost:1144/product/index.js?v=1"',
					'<div>',
					'"http://localhost:1144/product/index.js"</div>'
				].join('\n'),
				'Arquivo js apenas substituído fora da tag div SEM classname (definida pelo config)');
			t.end();
		});

		t.test('- não faz nada, quando version não é definido', function(t) {
			var couchModifiers = helper.getMockUpConfig();
			var result = couchModifiers.applyVersion({
				body: 'http://localhost:1144/product/index.js'
			});
			t.isEqual(result.body,
				'http://localhost:1144/product/index.js',
				'Arquivo js com substuição livre no parâmetro "version"');

			t.end();
		});
	});

	t.test('Faz o update da url na versão absoluta para meta tags', function(t) {
		var couchModifiers = helper.getMockUpConfig({
			relative: true,
			urlMeta: true
		});

		t.test('- inserção da meta url em meta tag', function(t) {
			var result = couchModifiers.updateMetaTagsUrl({
				body: '<meta property="og:image" content="./image/teste.jpg" />'
			});

			t.isEqual(result.body,
				'<meta property="og:image" content="http://www.site.com.br/image/teste.jpg" />',
				'Substitui a meta tag pelo www.site.com.br definido no config');

			t.end();
		});

		t.test('- inserção da meta url em meta tag (caminho relativo com 2 níveis)', function(t) {
			var result = couchModifiers.updateMetaTagsUrl({
				body: '<meta property="og:image" content="../../image/teste.jpg" />'
			});

			t.isEqual(result.body,
				'<meta property="og:image" content="http://www.site.com.br/image/teste.jpg" />',
				'Substitui a meta tag com caminho relativo pelo www.site.com.br definido no config');

			t.end();
		});

		t.test('- inserção da meta url em página listada no forceMetaUrl como STRING', function(t) {
			var result = couchModifiers.updateToProdUrl({
				body: '<div data-src="http://localhost:1144/image/teste.jpg" />',
				page: 'http://localhost:1144/sitemap.xml'
			});

			t.isEqual(result.body,
				'<div data-src="http://www.site.com.br/image/teste.jpg" />',
				'Força substituição do atributo pelo www.site.com.br definido no config');

			t.end();
		});

		t.test('- inserção da meta url em página listada no forceMetaUrl como REGEXP', function(t) {
			var result = couchModifiers.updateToProdUrl({
				body: '<div data-src="http://localhost:1144/image/teste.jpg" />',
				page: 'http://localhost:1144/teste.xml'
			});

			t.isEqual(result.body,
				'<div data-src="http://www.site.com.br/image/teste.jpg" />',
				'Força substituição do atributo pelo www.site.com.br definido no config como regexp');

			t.end();
		});

		t.test('- NÃO substitui a meta url em página não listada em forceMetaUrl', function(t) {
			var result = couchModifiers.updateToProdUrl({
				body: '<div data-src="http://localhost:1144/image/teste.jpg" />',
				page: 'http://localhost:1144/outra-pagina.xml'
			});

			t.isEqual(result.body,
				'<div data-src="./image/teste.jpg" />',
				'Não faz substituição pelo www.site.com.br definido no config');

			t.end();
		});
	});
});

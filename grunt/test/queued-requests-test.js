var tap = require('tape');

tap.test('-- QUEUED REQUESTS --', function(t) {

	t.test('Testa limite de conexões simultâneas', function(t) {
		var QueuedRequests = require('../modules/queued-requests');

		t.test('- requests adicionados e finalizados SEM próxima chamada em seguida', function(t) {
			var queue = new QueuedRequests();
			var timeout = queue.timeout * 2;

			for (var i=0; i<10; i++) {
				queue.add(function(){});
			}

			setTimeout(function() {
				t.isEqual(queue.iddle, 0, 'Iddle é 0 após adicionar 10 requests');

				queue.done(false);
				queue.done(false);

				t.isEqual(queue.iddle, 2, 'Iddle é 2 após finalizar 2 requests');

				queue.run();

				setTimeout(function() {
					t.isEqual(queue.iddle, 1, 'Iddle é 1 após rodar 1 request');
					t.end();
				}, timeout);

			}, timeout);
		});

		t.test('- requests adicionados e finalizados COM próxima chamada em seguida', function(t) {
			var queue = new QueuedRequests(2);
			var timeout = queue.timeout * 2;

			for (var i=0; i<10; i++) {
				queue.add(function(){});
			}

			setTimeout(function() {
				queue.done();
				queue.done();

				t.isEqual(queue.iddle, 2, 'Iddle é 2 após finalizar e iniciar 2 requests');
				t.end();
			}, timeout);
		});
	});
});

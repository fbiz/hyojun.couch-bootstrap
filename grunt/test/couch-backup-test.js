var tap = require('tape');

tap.test('-- COUCH BACKUP --', function(t) {
	t.test('Gera nome de arquivo para backup', function(t) {
		var couchBackup = require('../modules/couch-backup');
		var testDate = new Date(2015, 01, 10, 13, 30, 05);
		var url = 'http://teste.staging.aceite.fbiz.com.br';
		var zip = 'couch-bkp_teste.staging.aceite.fbiz.com.br_20150210-133005';

		t.isEqual(couchBackup.getFileName(url, testDate), zip,
			'Nome do zip segue formato AMBIENTE_YYYYMMDD-HHMMSS');

		t.end();
	});

	t.test('Lista de arquivos é ordenada pela data no nome', function(t) {
		var couchBackup = require('../modules/couch-backup');
		var fileList = [
			'localhost_20150210-133010.zip',
			'couch-bkp_teste.staging.aceite.fbiz.com.br_20150210-133000.zip',
			'localhost_20150210-133005.zip'
		];

		t.isEqual(couchBackup.sortFileList(fileList), 'localhost_20150210-133005.zip',
			'Pega último arquivo ZIP pela data no nome');

		t.end();
	});
});

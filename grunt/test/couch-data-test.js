var tap = require('tape');
var helper = require('./helpers/couch-data');

tap.test('-- COUCH DATA --', function(t) {
	t.test('Pega valores padrão', function(t) {
		var couchData = helper.getDataFromConfig();

		t.isEqual(couchData.port(), '1144', 'utiliza porta do couch padrão');
		t.isEqual(couchData.url(), 'http://localhost:1144/', 'utiliza url padrão');
		t.isEqual(couchData.urlProd(), './', 'utiliza url de "produção" padrão');
		t.isEqual(couchData.urlMeta(), 'http://localhost:1144/', 'utiliza url absoluta padrão');
		t.isEqual(couchData.user(), 'system', 'utiliza login padrão do couch');
		t.isEqual(couchData.password(), 'admin', 'utiliza senha padrão do couch');
		t.isEqual(couchData.dbUser(), 'root', 'utiliza usuário padrão do banco de dados');
		t.isEqual(couchData.dbPassword(), '159753', 'utiliza senha padrão do banco de dados');
		t.isEqual(couchData.dbHost(), 'localhost', 'utiliza host padrão do banco de dados');
		t.isEqual(couchData.dbName(), 'hyojun_couch_bootstrap', 'utiliza nome padrão do banco de dados');
		t.isEqual(couchData.sshKey(), '.vagrant/machines/default/virtualbox/private_key',
				'utiliza caminho de chave padrão para o SSH');
		t.isEqual(couchData.sshUrl(), '127.0.0.1', 'utiliza url padrão para o SSH');
		t.isEqual(couchData.sshPort(), '2222', 'utiliza porta padrão para o SSH');
		t.isEqual(couchData.sshUser(), 'vagrant', 'utiliza usuário padrão para o SSH');
		t.isEqual(couchData.sshRemotePath(), '/var/www/html', 'utiliza caminho remoto padrão para o SSH');
		t.isEqual(couchData.connectionLimits(), 5, 'utiliza limite de conexões padrão para baixar os assets');
		t.isEqual(couchData.extension(), '.html', 'utiliza extensão padrão para geração do site estático');
		t.isEqual(couchData.dest(), '_site/', 'utiliza caminho de destino padrão para geração do site estático');
		t.isEqual(couchData.qualitySmall(), 89, 'utiliza qualidade padrão para compressão de imagens pequenas');
		t.isEqual(couchData.qualityLarge(), 75, 'utiliza qualidade padrão para compressão de imagens grandes');

		t.end();
	});

	t.test('Pega valores de configuração', function(t) {
		var couchData = helper.getDataFromConfig();
		var data = {
			"sitemap/": "sitemap.xml"
		};
		t.deepEqual(couchData.config('static.convert'), data, 'pega valor static.convert do arquivo de configuração');
		t.isEqual(couchData.config('static.extension'), '.html', 'pega valor static.extension do arquivo de configuração');
		t.isEqual(couchData.config('teste.um.dois'), undefined, 'pega valor que não existe');
		t.end();
	});

	t.test('Pega valores da VM e do GIT', function(t) {
		var couchData = helper.getDataFromBash();
		var data = {
			port: '3333',
			'ssh-port': '4444'
		};
		t.isEqual(couchData.vmConfig().port, '3333', 'Porta é extraída dinamicamente da VM');
		t.isEqual(couchData.vmConfig()['ssh-port'], '4444', 'Porta é extraídas dinamicamente da VM (segundo teste vem do cache)');
		t.isEqual(couchData.dbName(), 'hyojun_couch_bootstrap', 'Nome do banco é extraído do GIT remote');
		t.end();
	});

	t.test('Cai no erro quando comando da VM não existe', function(t) {
		var couchData = helper.getDataFromBash({withError: true});
		t.deepEqual(couchData.vmConfig(), {}, 'Objeto deve ser vazio pois comando não foi encontrado');
		t.end();
	});

	t.test('Pega URL atualizada com a porta da VM', function(t) {
		var couchData = helper.getDataFromBash();
		t.isEqual(couchData.url(), 'http://localhost:3333/', 'URL é alterada para bater com o valor da porta da VM.');
		t.end();
	});
});

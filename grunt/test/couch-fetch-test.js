var tap = require('tape');
var helper = require('./helpers/couch-fetch');

tap.test('-- COUCH FETCH --', function(t) {

	t.test('Converte o nome do arquivo', function(t) {
		var couchFetch = helper.mockUpConvertName();

		t.test('- definido como STRING', function(t) {
			t.isEqual(couchFetch.convertFileName('contato/'), 'contact/index.html',
				'para valor "contato/"');

			t.isEqual(couchFetch.convertFileName('atracoes/'), 'attractions/sub/index.html',
				'para valor "atracoes/"');

			t.end();
		});

		t.test('- definido como REGEXP', function(t) {
			t.isEqual(couchFetch.convertFileName('produtos/produto-1.html'), 'products/produto-1.html',
				'para valor "produtos/produto-1"');

			t.isEqual(couchFetch.convertFileName('produtos/sub/produto-2.html'), 'products/sub/produto-2.html',
				'para valor "produtos/sub/produto-2"');

			t.end();
		});

		t.test('- não muda o nome do arquivo quando não está definido', function(t) {
			t.isEqual(couchFetch.convertFileName('outro-nome/produto-1.html'), 'outro-nome/produto-1.html',
				'para valor "outro-nome/produto-1"');

			t.end();
		});
	});

	t.test('Página é ignorada para download', function() {
		var couchFetch = require('../modules/couch-fetch');
		var ignoreList = [
			"/^globals/",
			"page-list/"
		];

		t.ok(couchFetch.isPageIgnored('page-list/', ignoreList), 'page-list/ como string é ignorado.');
		t.ok(couchFetch.isPageIgnored('globals/', ignoreList), 'globals/ como regExp é ignorado.');
		t.notOk(couchFetch.isPageIgnored('globals/'), 'ignoreList não existe.');

		t.end();
	});

	t.test('Enfileira os arquivos para leitura (crawler)', function(t) {
		var couchFetch = require('../modules/couch-fetch');
		var data = helper.mockUpQueueData('fetch-crawler.html');;

		t.test('- numa lista vazia', function() {
			var fileList = [];

			couchFetch.queueFilesToRead(data, fileList);

			t.deepEqual(fileList,
				[
					'http://localhost:1144/assets/css/structure.css',
					'http://localhost:1144/assets/css/structure-2.css'
				],
				'encontra structure.css para que seja lido no futuro');

			t.end();
		});

		t.test('- numa lista com item existente', function(t) {
			var fileList = ['http://localhost:1144/assets/css/structure-2.css'];

			couchFetch.queueFilesToRead(data, fileList);

			t.deepEqual(fileList,
				[
					'http://localhost:1144/assets/css/structure-2.css',
					'http://localhost:1144/assets/css/structure.css'
				],
				'encontra structure.css, mas só adiciona os que não existem na lista para serem lidos no futuro');

			t.end();
		});
	});

	t.test('Enfileira os assets para download', function(t) {
		var couchFetch = helper.mockUpQueueAssets();
		var data = helper.mockUpQueueData('fetch-crawler.html');

		t.test('- numa lista vazia', function(t) {
			var fileList = [];
			couchFetch.queueAssetsToDownload(data, fileList);

			t.deepEqual(fileList,
				[
					'http://localhost:1144/assets/file/teste.json',
					'http://localhost:1144/assets/image/teste.jpg',
					'http://localhost:1144/assets/image/teste-2.jpeg',
					'http://localhost:1144/assets/image/teste-3.jpg',
					'http://localhost:1144/assets/image/teste-4.jpg'
				],
				'encontra arquivo jpg e json para que seja baixado no futuro');

			t.end();
		});

		t.test('- numa lista com item existente', function(t) {
			var fileList = ['http://localhost:1144/assets/image/teste-2.jpeg'];

			couchFetch.queueAssetsToDownload(data, fileList);

			t.deepEqual(fileList,
				[
					'http://localhost:1144/assets/image/teste-2.jpeg',
					'http://localhost:1144/assets/file/teste.json',
					'http://localhost:1144/assets/image/teste.jpg',
					'http://localhost:1144/assets/image/teste-3.jpg',
					'http://localhost:1144/assets/image/teste-4.jpg'
				],
				'encontra arquivo jpeg e json, mas só adiciona os que não existem na lista');

			t.end();
		});
	});

	t.test('Enfileira as imagens originais (não redimensionadas) para download', function(t) {
		var couchFetch = require('../modules/couch-fetch');
		var fileList = [
			'http://localhost:1144/assets/image/imagem1.jpg',
			'http://localhost:1144/assets/image/imagem2-100x150.jpg',
			'http://localhost:1144/assets/image/imagem3-250x100.jpg'
		];

		couchFetch.queueOriginalImagesToDownload(fileList);

		t.deepEqual(fileList,
			[
				'http://localhost:1144/assets/image/imagem1.jpg',
				'http://localhost:1144/assets/image/imagem2-100x150.jpg',
				'http://localhost:1144/assets/image/imagem3-250x100.jpg',
				'http://localhost:1144/assets/image/imagem2.jpg',
				'http://localhost:1144/assets/image/imagem3.jpg'
			],
			'adiciona arquivos imagem2 e imagem3 originais');

		t.end();
	});

	t.test('Compara data do arquivo local com o arquivo remoto', function(t) {
		t.test('- local é mais novo', function(t) {
			var couchFetch = helper.mockUpCompareDates({
				localDate: 'Mon Nov 23 2015 11:30:00 GMT-0200 (BRST)' // local GMT-0200 (brasil horáriod e verão)
			});
			var now = new Date('Mon Nov 23 2015 12:00:00 GMT-0200 (BRST)');
			var response = {
				headers: {
					'date': 'Mon Nov 23 2015 14:00:00 GMT', // data servidor GMT
					'last-modified': 'Mon Nov 23 2015 13:15:00 GMT'
				}
			};

			t.notOk(couchFetch.downloadedAssetIsNewer(null, response, now), 'arquivo local é mais novo');
			t.end();
		});

		t.test('- local é mais velho', function(t) {
			var couchFetch = helper.mockUpCompareDates({
				localDate: 'Mon Nov 23 2015 12:00:00 GMT'
			});
			var now = new Date('Mon Nov 23 2015 12:30:00 GMT');
			var response = {
				headers: {
					'date': 'Mon Nov 23 2015 12:30:00 GMT',
					'last-modified': 'Mon Nov 23 2015 12:01:00 GMT'
				}
			};

			t.ok(couchFetch.downloadedAssetIsNewer(null, response, now), 'arquivo remoto é mais novo');
			t.end();
		});

		t.test('- arquivo não existe', function(t) {
			var couchFetch = helper.mockUpCompareDates();

			t.ok(couchFetch.downloadedAssetIsNewer('arquivo-de-teste.php', null, null), 'sempre retorna true');
			t.end();
		});
	});
});

/* jshint maxlen: false */
var couchFetchModifiers = function(mock) {

	mock = mock || {};
	mock.couch = mock.couch || {};

	var grunt = require('grunt');
	var url = require('url');
	var util = require('util');
	var api = {};
	var couch = {};

	couch.data = mock.couch.data || require('./couch-data');
	couch.utils = mock.couch.utils || require('./couch-utils');

	var config = couch.utils.getConfig();

	/* istanbul ignore next */
	api.map = function(fnc) {
		return function(data) {
			return (typeof fnc === 'function') ? data.map(fnc.bind(this)) : data;
		}.bind(this);
	};

	// helpers
	api.matchMetaUrl = function(file) {
		var isMatched = false;

		(couch.data.config('static.forceMetaUrl') || []).forEach(function(val) {
			if (isMatched) {
				return;
			}
			if (couch.utils.isRegExp(val)) {
				isMatched = (new RegExp(val.slice(1, -1))).test(file);
			} else {
				isMatched = (val === file);
			}
		});

		return isMatched;
	};

	// modifiers
	api.applyVersion = function(data) {
		var version = couch.data.version();

		if (!version) {
			return data;
		}

		var content = data.body;
		var scriptTempReplacement = '__couch_script_match__';
		var scriptRegExpString = '(<(' + couch.data.config('static.version.ignoreTags').join('|') + ')(?:.|[\\n\\r])*?>)((?:.|[\\n\\r])*?)(<\\/\\2>)';
		var scriptRegExp = new RegExp(scriptRegExpString, 'g');
		var scriptList = content.match(scriptRegExp);

		var removeTagsFromContent = function(content) {
			return content.replace(scriptRegExp, '$1' + scriptTempReplacement + '$4');
		};

		var applyLiteralVersionOnContent = function(content) {
			return content.replace(new RegExp(couch.data.config('static.version.replace'), 'g'), version);
		};

		var applyVersionOnContent = function(content) {
			var extRegExp = new RegExp(util.format(couch.utils.extensionRegExp, couch.data.config('static.version.ext').join('|')), 'gmi');
			var matches = content.match(extRegExp) || [];
			var returnValue = content + '';

			matches.forEach(function(val, i) {
				var urlValue = val.match(/^url\(/) ? val.slice(4, -1) : val.slice(1, -1);
				var urlData = url.parse(urlValue, true);
				var group1 = '\\' + (val.match(/^url\(/) ? couch.utils.urlToRegExp(val.slice(0, 4)) : val.slice(0, 1));
				var group2 = '\\' + val.slice(-1);

				var regExp = util.format('(%s)%s(%s)', group1, couch.utils.urlToRegExp(urlValue), group2);

				delete urlData.search;
				urlData.query = urlData.query || {};
				urlData.query.v = version;

				returnValue = returnValue.replace(new RegExp(regExp), '$1' + url.format(urlData) + '$2');
			});

			return returnValue;
		};

		function reApplyRemovedTagsOnContent(content) {
			var matches = content.match(new RegExp(scriptTempReplacement, 'g')) || [];
			var returnValue = content + '';
			var scriptRegExp = new RegExp(scriptRegExpString);

			matches.forEach(function(val, i) {
				var scriptContent = scriptList[i].match(scriptRegExp);
				returnValue = returnValue.replace(scriptTempReplacement, scriptContent[3]);
			});

			return returnValue;
		}

		content = removeTagsFromContent(content);
		content = applyLiteralVersionOnContent(content);
		content = applyVersionOnContent(content);
		content = reApplyRemovedTagsOnContent(content);

		data.body = content;

		return data;
	};

	api.updateToProdUrl = function(data) {
		var file = (data.redirect || data.page).replace(couch.data.url(), '');
		var urlProd = this.getRelativeProdPath(file);
		var urlAsRegExp = couch.utils.urlToRegExp(couch.data.url());

		if (this.matchMetaUrl(file)) {
			urlProd = couch.data.urlMeta();
		}

		data.body = data.body.replace(new RegExp(urlAsRegExp, 'g'), urlProd);

		return data;
	};

	api.updateMetaTagsUrl = function(data) {
		var metaRegExp = new RegExp('(<meta.*?)(?:\\.+\\/)+(.*?>)', 'g');
		data.body = data.body.replace(metaRegExp, '$1' + couch.data.urlMeta() + '$2');
		return data;
	};

	api.getRelativeProdPath = function(file) {
		var urlProd = couch.data.urlProd();
		var rel = [];

		if (urlProd.match(/^\./)) {
			rel.length = file.split('/').length;

			return rel.join('../') || urlProd;
		}

		return urlProd;
	};

	/* istanbul ignore next */
	api.beforeParsePage = function(data) {
		var parse;
		if (grunt.option('before-parse')) {
			parse = require('../' + grunt.option('before-parse'));
			return parse(data);
		}
		return data;
	};

	api.convertFileNameInPage = function(data) {
		var convert = couch.data.config('static.convert');
		var keys = Object.keys(convert);
		var body = data.body;
		var file = data.page;

		keys.forEach(function(val) {
			var newUrl = null;
			var regExp = null;

			if (!couch.utils.isRegExp(val)) {
				newUrl = url.resolve(couch.data.url(), val);
				regExp = new RegExp(couch.utils.urlToRegExp(newUrl), 'g');
			} else {
				regExp = new RegExp(couch.utils.urlToRegExp(couch.data.url()) + val.slice(1,-1), 'g');
			}
			body = body.replace(regExp, url.resolve(couch.data.url(), convert[val]));
		});

		data.body = body;

		return data;
	};

	return api;
};

module.exports = couchFetchModifiers();
module.exports.mock = function() {
	return couchFetchModifiers.apply(null, arguments);
};

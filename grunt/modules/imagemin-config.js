var grunt = require('grunt');
var path = require('path');
var couch = require('./couch');

module.exports = {
	generate: function() {
		var imgSize = require('image-size');
		var mozjpeg = require('imagemin-mozjpeg');
		var fileList = grunt.file.expand({}, ['assets/image/**/*.{png,jpg,jpeg,gif}']);
		var imgSmallList = {};
		var imgLargeList = {};

		fileList.forEach(function(val) {
			try {
				var size = imgSize(val);
			} catch(e) {
				var size = {width: 0, height: 0};
			}

			var imgPath = path.join(couch.data.dest(), val);

			if ((size.width * size.height) > (450 * 450)) {
				imgLargeList[imgPath] = val;
			} else {
				imgSmallList[imgPath] = val;
			}
		});

		return {
			small: {
				options: {
					use: [ mozjpeg({ quality: couch.data.qualitySmall(), quantTable: 2 }) ]
				},
				files: imgSmallList
			},

			large: {
				options: {
					use: [ mozjpeg({ quality: couch.data.qualityLarge(), quantTable: 2 }) ]
				},
				files: imgLargeList
			}
		}
	}
};

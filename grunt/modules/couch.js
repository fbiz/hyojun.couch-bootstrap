function couch(mock) {
	mock = mock || {};

	return {
		backup: mock.backup || require('./couch-backup'),
		build: mock.build || require('./couch-build'),
		connections: mock.connections || require('./couch-connections'),
		data: mock.data || require('./couch-data'),
		fetch: mock.fetch || require('./couch-fetch'),
		logger: mock.logger || require('./couch-logger'),
		modifiers: mock.modifiers || require('./couch-modifiers'),
		utils: mock.utils || require('./couch-utils'),
		route: mock.route || require('./couch-route')
	}
};

module.exports = couch();
module.exports.mock = function() {
	return couch.apply(null, arguments);
};

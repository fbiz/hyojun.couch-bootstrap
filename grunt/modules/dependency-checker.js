module.exports = function(grunt) {

	var madge = require('madge');

	function DependencyChecker(p_config) {
		p_config = p_config || {};

		var list = [],
			appRoot = p_config.appRoot || 'app/',
			root = (p_config.root || '.').replace(/([^\/])$/, '$1/'),
			exclude = p_config.exclude || 'dist/*',
			madgeConfig = {
				format: 'amd',
				exclude: exclude
			};

		this.getDependenciesFrom = function(file) {
			var path = root + file + '.js',
				tree = null;

			if (grunt.file.exists(path)) {
				return madge(path, madgeConfig).tree;
			}
			return null;
		};

		this.getAllDependenciesFrom = function(file) {
			var result = this.getDependenciesFrom(file);

			if (result != null && list.indexOf(file) < 0 ) {
				list.push(file);

				result[file.split('/').pop()].forEach(function(val) {
					this.getAllDependenciesFrom(val);
				}.bind(this));
			}

			return list;
		};

		this.getRelatedTo = function(module) {
			return madge(root, madgeConfig).depends(module);
		};

		this.getAllRelatedTo = function(module) {
			var ret = [];
			this.getRelatedTo(module).forEach(function(val) {
				ret.push(val);
				ret = ret.concat(this.getRelatedTo(val));
			}.bind(this));

			return ret.filter(function(val, i, arr){
				return arr.indexOf(val) === i;
			});
		};

		this.getAllAppRelatedTo = function(module) {
			return this.getAllRelatedTo(module)
				.filter(function(val, i, arr) {
					return val.match(new RegExp('^' + appRoot)) != null;
				})
				.map(function(val) {
					return val.replace(new RegExp('^' + appRoot), '');
				});
		};

		this.convertFileToModule = function(file) {
			return file.replace(/\.js$/, '').split(root).pop();
		};

		this.isFileApp = function(file) {
			file = file.replace(/\.js$/, '').split(root).pop();
			return !!file.match(new RegExp('^' + appRoot), '');
		};
	}

	return DependencyChecker;
};

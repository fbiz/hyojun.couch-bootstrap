var couchBuild = function(mock) {

	mock = mock || {};
	mock.couch = mock.couch || {};

	var grunt = require('grunt');
	var path = require('path');

	var couch = {};
	couch.data = mock.couch.data || require('./couch-data');
	couch.logger = mock.couch.logger || require('./couch-logger');
	couch.connections = mock.couch.connections || require('./couch-connections');

	return {
		getPHPFiles: function() {
			return new Promise(function(ok, fail) {
				try {
					var files = grunt.file.expand({
						filter: function(file) {
							return /COUCH::invoke/.test(grunt.file.read(file));
						}
					}, [
						"**/*.php",
						"!bkp/**/*.php",
						"!_site/**/*.php",
						"!couch/**/*.php",
						"!node_modules/**/*.php",
						"!bower_components/**/*.php",
						"!includes/**/*.php"
					]);

					grunt.verbose.writeln([
						'Páginas relacionadas com o CouchCMS:',
						files.join('\n')
					].join('\n'));

					ok(files);
				} catch(e) {
					fail(e);
				}
			}.bind(this));
		},

		getPHPFilesAsUrl: function() {
			return this.getPHPFiles().then(function(files) {
				files = files.map(function(val) {
					return couch.data.url() + val;
				}.bind(this));

				return files;
			})
		},

		build: function(file) {
			if (file) {
				var dest = couch.data.url() + file.replace(process.cwd() + path.sep, '');
				return couch.connections.login().then(function() {
					return couch.connections.request(dest);
				});
			}

			return this.getPHPFilesAsUrl()
				.then(function(pageList) {
					return couch.connections.multipleRequest(pageList);
				});
		}
	};
};

module.exports = couchBuild();
module.exports.mock = function() {
	return couchBuild.apply(null, arguments);
};

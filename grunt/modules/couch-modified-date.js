var couchModifiedDate = function(mock) {

	mock = mock || {};

	var childProccess = mock.childProccess || require('child_process');
	var exec = childProccess.execSync;
	var fs = mock.fs || require('fs');
	var api = {};

	api.syncModifiedDate = function() {

		try {
			var fileList = exec('git ls-files assets/image assets/file').toString().split('\n');
		} catch(e) {
			var fileList = [];
		}

		fileList.forEach(function(val) {
			var gitLastModified = exec('git log --pretty=format:%cd -n 1 --date=iso ' + val);
			if (fs.existsSync(val)) {
				var date = new Date(gitLastModified.toString());
				fs.utimesSync(val, date, date);
			}
		})
	};

	return api;
}

module.exports = couchModifiedDate();
module.exports.mock = function() {
	return couchBuild.apply(null, arguments);
};

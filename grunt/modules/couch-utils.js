var couchUtils = function(mock) {

	mock = mock || {};
	var grunt = require('grunt');
	var api = {};

	api.extensionRegExp = '("|\'|url\\()[^"\'\\(]+\\.(%s)(?:[\?#].*?|)(\\1|\\))';

	/* istanbul ignore next */
	api.getConfig = function() {
		if (grunt.file.exists('go-static.config.json')) {
			return grunt.file.readJSON('go-static.config.json');
		}

		return {
			convert: {},
			ignore: [],
			version: {
				replace:'',
				ext: []
			},
			download: {
				ext: []
			}
		};
	};

	api.cloneObj = function(data) {
		/* istanbul ignore next */
		if (!data) {
			return null;
		}
		return JSON.parse(JSON.stringify(data));
	};

	api.isRegExp = function(val) {
		return typeof val === 'string'
			&& val.slice(0,1) == '/'
			&& val.slice(-1) == '/';
	};

	api.urlToRegExp = function(val) {
		return val && val.replace(/([\/\?\.\(\)])/g, '\\$1');
	};

	return api;
};

module.exports = couchUtils();
/* istanbul ignore next */
module.exports.mock = function() {
	return couchUtils.apply(null, arguments);
};

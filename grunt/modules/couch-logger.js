function couchLogger(logger) {

	var baseLogger = {
		verbose: function() {
			console.log.apply(console, arguments);
		},
		log: function() {
			console.log.apply(console, arguments);
		},
		warn: function() {
			console.log.apply(console, arguments);
		},
		error: function() {
			console.log.apply(console, arguments);
		}
	}

	switch(true) {
		case (!logger):
		case (typeof logger.log != 'function'):
		case (typeof logger.warn != 'function'):
		case (typeof logger.error != 'function'):
			logger = baseLogger;
			break;
	}

	return logger;
}

module.exports = couchLogger();
module.exports.mock = function() {
	return couchLogger.apply(null, arguments);
};

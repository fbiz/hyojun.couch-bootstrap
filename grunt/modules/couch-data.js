var couchData = function(mock) {

	mock = mock || {};
	mock.couch = mock.couch || {};

	var grunt = mock.grunt || require('grunt');
	var childProccess = mock.childProccess || require('child_process');
	var url = mock.url || require('url');
	var path = mock.path || require('path');
	var vmConfig = null;
	var config = mock.config;
	var vmID = null;
	var couch = {};

	couch.logger = mock.couch.logger || require('./couch-logger');
	couch.utils = mock.couch.utils || require('./couch-utils');

	var api = {};

	api.vmID = function() {
		if (vmID) {
			return vmID;
		}

		var vmPath = '.vagrant/machines/default/virtualbox/id';
		if (grunt.file.exists(vmPath)) {
			vmID = grunt.file.read(vmPath);
		}

		return vmID;
	};
	api.checkVM = function() {
		if (!this.vmID()) {
			return vmConfig || {};
		}

		try {
			var run = childProccess.execSync;
			var stdout = run('VboxManage showvminfo ' + this.vmID()).toString();

			var matches = stdout.match(/^NIC \d Rule\(\d\).*$/gm);
			vmConfig = {};

			matches.forEach(function(val) {
				var name = val.match(/name = ([\w\d]+)/).pop();
				var host = val.match(/host port = ([\w\d]+)/).pop();

				switch(name) {
					case 'http':
						vmConfig.port = host;
						break;
					case 'ssh':
						vmConfig['ssh-port'] = host;
						break;
				}
			});
		} catch(e) {
			couch.logger.error('O comando "VboxManage" não existe ou retornou erro. Erro original:\n' + e);
		}

		return vmConfig || {};
	};
	api.vmConfig = function() {
		if (vmConfig) {
			return vmConfig;
		}

		return this.checkVM();
	};
	api.config = function(value) {
		if (!config) {
			/* istanbul ignore else */
			if (grunt.file.exists('couch.config.json')) {
				config = grunt.file.readJSON('couch.config.json');
			} else {
				throw new Error('Arquivo couch.config.json faltando na raiz do projeto.');
			}
		}

		var objPath = (value || '').split('.');
		var returnValue = couch.utils.cloneObj(config);

		objPath.forEach(function(val) {
			if (returnValue && returnValue[val]) {
				returnValue = returnValue[val];
			} else {
				returnValue = undefined;
			}
		});

		return returnValue;
	};

	api.port = function() {
		return grunt.option('port') || this.vmConfig().port || this.config('fetch.port');
	};
	api.url = function() {
		/* istanbul ignore next */
		if (grunt.option('url')) {
			return grunt.option('url');
		}

		var port = this.vmConfig().port;

		if (port && port != this.config('fetch.port')) {
			var objUrl = url.parse(this.config('fetch.url'));
			objUrl.port = port;
			delete objUrl.host;
			delete objUrl.href;

			return url.format(objUrl);
		}

		return this.config('fetch.url');
	};
	api.urlProd = function() {
		return grunt.option('url-prod') || this.config('static.urlProd');
	};
	api.urlMeta = function() {
		return grunt.option('url-meta') || this.config('static.urlMeta') || this.url();
	};
	api.user = function() {
		return grunt.option('user') || this.config('fetch.user');
	};
	api.password = function() {
		return grunt.option('password') || this.config('fetch.password');
	};

	api.dbUser = function() {
		return grunt.option('db-user') || this.config('db.user');
	};
	api.dbPassword = function() {
		return grunt.option('db-password') || this.config('db.password');
	};
	api.dbHost = function() {
		return grunt.option('db-host') || this.config('db.host');
	};
	api.dbName = function() {
		var getPath = function() {
			var run = childProccess.execSync;
			var name = null;
			try {
				name = run('git remote -v 2> /dev/null').toString('utf8');
				name = (name.match(/\/(.[^\/]*)\.git/) || [name]).pop();
				name = name.replace(/[^a-zA-Z0-9]/g, '_');
			} catch (e) {}

			return name;
		};

		return grunt.option('db-name') || this.config('db.name') || getPath();
	};

	api.sshKey = function() {
		return grunt.option('ssh-key') || this.config('ssh.keyPath');
	};
	api.sshUrl = function() {
		return grunt.option('ssh-url') || this.config('ssh.url');
	};
	api.sshPort = function() {
		return grunt.option('ssh-port') || this.vmConfig()['ssh-port'] || this.config('ssh.port');
	};
	api.sshUser = function() {
		return grunt.option('ssh-user') || this.config('ssh.user');
	};
	api.sshRemotePath = function() {
		return grunt.option('ssh-remote-path') || this.config('ssh.remotePath');
	};

	/* istanbul ignore next */
	api.tcApiUrl = function() {
		return grunt.option('tc-api-url');
	};
	/* istanbul ignore next */
	api.tcBuildId = function() {
		return grunt.option('tc-build-id');
	};
	/* istanbul ignore next */
	api.tcProjectId = function() {
		return grunt.option('tc-project-id');
	};
	/* istanbul ignore next */
	api.tcAuth = function() {
		return grunt.option('tc-auth');
	};
	/* istanbul ignore next */
	api.tcConfirm = function() {
		return grunt.option('tc-confirm');
	};

	/* istanbul ignore next */
	api.bkpPath = function() {
		return 'bkp' + path.sep;
	};
	api.connectionLimits = function() {
		return grunt.option('connection-limit') || this.config('fetch.connectionLimits');
	};
	api.extension = function() {
		return grunt.option('extension') || this.config('static.extension');
	};
	api.dest = function() {
		return path.normalize(grunt.option('dest') || this.config('static.dest')) + path.sep;
	};
	api.version = function() {
		return grunt.option('ver') || '';
	};
	api.qualitySmall = function() {
		return grunt.option('quality-small') || this.config('static.qualitySmall');
	};
	api.qualityLarge = function() {
		return grunt.option('quality-large') || this.config('static.qualityLarge');
	}

	return api;
};

module.exports = couchData();
module.exports.mock = function() {
	return couchData.apply(null, arguments);
};

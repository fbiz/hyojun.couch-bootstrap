function couchBackup(mock) {

	mock = mock || {};
	mock.couch = mock.couch || {};

	var archiver = mock.archiver || require('archiver');
	var unzip = mock.unzip || require('unzip2');
	var moment = mock.moment || require('moment');
	var fs = mock.fs || require('fs');
	var url = mock.url || require('url');
	var couch = {};

	couch.data = mock.couch.data || require('./couch-data');
	couch.logger = mock.couch.logger || require('./couch-logger');

	var dateFormat = 'YYYYMMDD-HHmmss';
	var api = {};

	api.getFileName =  function(finalUrl, date) {
		var date = moment(date).format(dateFormat);
		var host = url.parse(finalUrl).hostname;
		var name = ['couch-bkp'];

		if (host) {
			name.push(host);
		}
		name.push(date);
		name = name.join('_');
		return name;
	};

	/* istanbul ignore next */
	api.generate =  function() {
		return new Promise(function(ok, fail) {
			var archive = archiver.create('zip');
			var name = this.getFileName(couch.data.url(), moment());
			var output = fs.createWriteStream(name + '.zip');

			couch.logger.verbose('Criando ZIP: ' + name);

			archive.pipe(output);
			archive.bulk([
				{
					expand: true,
					cwd: couch.data.bkpPath(),
					src: '**/*',
					dest: '.'
				}
			]);
			archive.finalize();

			output.on('close', function() {
				ok('ZIP criado em: ' + name);
			});
			output.on('error', function(err) {
				fail([
					'Não foi possível criar o arquivo ZIP. Erro original:',
					typeof err === 'object' ? JSON.stringify(err) : err
				].join('\n'));
			});
		}.bind(this));
	};

	api.sortFileList =  function(file) {
		file = file.sort(function(a, b) {
			a = moment(a.match(/\d{8}-\d{6}/).pop(), dateFormat);
			b = moment(b.match(/\d{8}-\d{6}/).pop(), dateFormat);
			return a > b;
		});

		return file.slice(-2, -1).pop();
	};

	/* istanbul ignore next */
	api.restore =  function(file) {
		return new Promise(function(ok, fail) {

			var extract = unzip.Extract({
				path: '.'
			});

			if (file && file.constructor === Array) {
				file = this.sortFileList(file);
			}

			if (!file) {
				couch.logger.error([
					'Não foi possível encontrar um arquivo zip no formato couch-bkp_*.zip.',
					'Para apontar um arquivo zip, utilize o parâmetro --file.'
				].join('\n'))
				return fail();
			} else {
				couch.logger.verbose('Extraindo o arquivo ', file);
			}

			fs.createReadStream(file).pipe(extract);

			extract.on('close', ok);
			extract.on('error', function(err) {
				couch.logger.error([
					'Não foi possível extrair o arquivo ZIP. Erro original:',
					typeof err === 'object' ? JSON.stringify(err) : err
				].join('\n'));
				fail();
			});
		}.bind(this));
	};

	return api;
};

module.exports = couchBackup();
/* istanbul ignore next */
module.exports.mock = function() {
	return couchBackup.apply(null, arguments);
};
function couchRoute(mock) {

	mock = mock || {};
	mock.couch = mock.couch || {};

	var grunt = require('grunt');
	var couch = {};

	couch.utils = mock.couch.utils || require('./couch-utils');

	var Pipeline = function(data) {
		this.data = couch.utils.cloneObj(data);

		this.pipe = function(fnc) {
			this.data = (typeof fnc === 'function') ? fnc(couch.utils.cloneObj(this.data)) : this.data;
			return this;
		};
	};

	var api = {
		regExpDumpBase64: '(;s:)(\\d+)(:.*?")(.*?)(\\\\)',
		Pipeline: Pipeline
	};

	/* istanbul ignore next */
	api.map = function(fnc) {
		return function(data) {
			return (typeof fnc === 'function') ? data.map(fnc.bind(this)) : data;
		}.bind(this);
	};

	/* istanbul ignore next */
	api.searchFiles = function(from) {
		return grunt.file.expand({}, [
			"**/*.php",
			"!bkp/**/*.php",
			"!_site/**/*.php",
			"!couch/**/*.php",
			"!node_modules/**/*.php",
			"!bower_components/**/*.php",
		]);
	};

	/* istanbul ignore next */
	api.readFiles = function(files, from, to) {
		var data = [];
		files.forEach(function(val) {
			data.push({
				page: val,
				from: from,
				to: to,
				write: false,
				body: grunt.file.read(val)
			});
		});

		return new Pipeline(data);
	};

	/* istanbul ignore next */
	api.replaceFile = function(data) {
		if (data.write === true) {
			grunt.file.write(data.page, data.body);
		}

		return data;
	};

	/* istanbul ignore next */
	api.moveFile = function(data) {
		if (data.page === data.from) {
			grunt.file.copy(data.from, data.to);
			grunt.file.delete(data.from);
		}
	};

	api.replaceContent = function(data) {
		var newData = couch.utils.cloneObj(data);
		var attributes = ['masterpage'].join('|');
		var regExp = new RegExp('(' + attributes + ')="' + couch.utils.urlToRegExp(data.from) + '"', 'g');
		var generateVarName = function(name) {
			return 'url_' + name
				.replace(/[\/]/g, '_')
				.replace(/\.(php|html)/g, '');
		};

		newData.body = newData.body.replace(regExp, '$1="' + data.to + '"');
		newData.body = newData.body.replace(new RegExp(generateVarName(data.from), 'g'), generateVarName(data.to));

		if (newData.body != data.body) {
			newData.write = true;
		}

		return newData;
	};

	/* istanbul ignore next */
	api.readDump = function(dumpPath, from, to) {
		var data = {
			page: dumpPath,
			from: from,
			to: to,
			write: false,
			body: ''
		};
		if (grunt.file.exists(dumpPath)) {
			data.body = grunt.file.read(dumpPath);
		}
		return new Pipeline(data);
	};

	api.replaceDumpBase64 = function(regExpToMatch) {
		return function(data) {
			var newData = couch.utils.cloneObj(data);
			var replaceRegExp = new RegExp(couch.utils.urlToRegExp(data.from), 'g');
			var matchRegExp = new RegExp(regExpToMatch);
			var matches = newData.body.match(new RegExp(regExpToMatch, 'g')) || [];

			matches.forEach(function(val) {
				var match = val.match(matchRegExp);
				var decodedValue = new Buffer(match[4], 'base64').toString('utf-8');
				var encodedValue = '';

				if (!decodedValue.match(replaceRegExp)) {
					return;
				}

				encodedValue = new Buffer(decodedValue.replace(replaceRegExp, data.to)).toString('base64');

				newData.write = true;
				newData.body = newData.body.replace(val, [
					match[1],
					encodedValue.length,
					match[3],
					encodedValue,
					match[5]
				].join(''));
			});

			return newData;
		}
	};

	api.replaceDumpContent = function(data) {
		var newData = couch.utils.cloneObj(data);
		var regExp = new RegExp('\'' + couch.utils.urlToRegExp(data.from) + '\'', 'g');

		newData.body = newData.body.replace(regExp, '\'' + data.to + '\'');

		if (data.body != newData.body) {
			newData.write = true;
		}

		return newData;
	};

	/* istanbul ignore next */
	api.rename = function(from, to) {
		var files = this.searchFiles(from) || [];
		var matches = files.filter(function(val) {
			return val === from;
		});

		if (matches.length === 0) {
			return;
		}

		this.readFiles(files, from, to)
			.pipe(this.map(this.replaceContent))
			.pipe(this.map(this.replaceFile))
			.pipe(this.map(this.moveFile));

		this.readDump('couch/install-ex.php', from, to)
			.pipe(this.replaceDumpContent)
			.pipe(this.replaceDumpBase64(this.regExpDumpBase64))
			.pipe(this.replaceFile);
	};

	return api;
};

module.exports = couchRoute();
/* istanbul ignore next */
module.exports.mock = function() {
	return couchRoute.apply(null, arguments);
};
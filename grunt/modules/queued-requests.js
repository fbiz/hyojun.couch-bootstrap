function QueuedRequests(limit) {
	this.iddle = limit || 5;
	this.data = [];
	this.timeout = 100;
};

QueuedRequests.prototype = {
	add: function(req) {
		this.data.push(req);
		this.run();
	},

	run: function() {
		setTimeout(function() {
			if (typeof this.data[0] === 'function' && this.iddle > 0) {
				this.iddle--;
				this.data[0]();
				this.data.splice(0,1);
			}
		}.bind(this), this.timeout);
	},

	done: function(runNext) {
		this.iddle++;
		if (runNext !== false) {
			this.run();
		}
	}
};

QueuedRequests.lazy = function(limit) {
	return new QueuedRequests(limit);
};

module.exports = QueuedRequests;

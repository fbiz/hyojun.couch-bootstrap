var couchConnections = function(mock) {

	mock = mock || {};
	mock.couch = mock.couch || {};

	var grunt = mock.grunt || require('grunt');
	var request = mock.request || require('request');
	request = request.defaults({jar: true});

	var couch = {};
	couch.data = mock.couch.data || require('./couch-data');
	couch.logger = mock.couch.logger || require('./couch-logger');

	var queue = mock.queue || require('./queued-requests').lazy(couch.data.connectionLimits());

	return {
		request: function(opt) {

			return new Promise(function(ok, fail) {
				queue.add(function() {

					grunt.verbose.writeln('Requesting: ', opt);
					request(opt, function(err, res, body) {
						queue.done();
						if (!err && ((res.statusCode >= 200 && res.statusCode < 400) || res.statusCode === 404)) {
							if (res.statusCode === 404) {
								couch.logger.log([
									'> ' + opt + ' será ignorado por não ter sido encontrado.',
									'  Se a ação foi proposital, acesse o admin e remova o template.',
									'  Se não, verifique se a VM está rodando e se as portas estão corretas.'
								].join('\n'));
							}
							return ok(res);
						}
						return fail({
							err: err,
							res: res,
							body: body
						});
					});
				});
			});
		},

		login: function() {
			return new Promise(function(ok, fail) {
				var data = {
					uri: couch.data.url() + 'couch/login.php',
					qs: {
						redirect: '/couch/adm.php'
					},
					method: 'POST',
					headers: {
						'Cookie': 'couchcms_testcookie=CouchCMS+test+cookie'
					},
					form: {
						'k_user_name': couch.data.user(),
						'k_user_pwd': couch.data.password(),
						'k_cookie_test': '1',
						'k_login': 'Login'
					}
				};
				this.request(data)
					.then(ok)
					.catch(fail);
			}.bind(this));
		},

		install: function() {
			var data = {
				uri: couch.data.url(),
				method: 'POST',
				form: {
					'name': couch.data.user(),
					'password': couch.data.password(),
					'repeat_password': couch.data.password(),
					'email': 'admin@admin.com',
					'k_hid_frm_login': 'frm_login'
				}
			};

			return this.request(data);
		},

		getDump: function() {
			return this.login()
				.then(function() {
					return this.request(couch.data.url() + 'couch/gen_dump.php');
				}.bind(this))
				.catch(couch.logger.error);
		},

		getPageList: function() {
			return this.login().then(function() {
				return this.request(couch.data.url() + 'page-list/')
					.then(function(res) {
						var pageList = res.body.match(/page:.*?$/gm) || [];
						pageList = pageList.map(function(val) {
							return val.replace(/^page:/, '');
						});
						// grunt.verbose.writeln('Páginas registradas no CouchCMS: ' + pageList);
						return pageList;
					});
			}.bind(this));
		},

		multipleRequest: function(pageList) {
			return this.login().then(function() {
				pageList = pageList.map(function(val) {
					return this.request(val);
				}.bind(this));

				return Promise.all(pageList);
			}.bind(this));
		}
	};
};

module.exports = couchConnections();
module.exports.mock = function() {
	return couchConnections.apply(null, arguments);
};

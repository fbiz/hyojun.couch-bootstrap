var couchFetch = function(mock) {

	mock = mock || {};
	mock.couch = mock.couch || {};

	var path = require('path');
	var grunt = mock.grunt || require('grunt');
	var fs = mock.fs || require('fs');
	var url = require('url');
	var util = require('util');
	var downloadList = [];
	var fileList = [];
	var couch = {};

	couch.logger = mock.couch.logger || require('./couch-logger');
	couch.connections = mock.couch.connections || require('./couch-connections');
	couch.data = mock.couch.data || require('./couch-data');
	couch.utils = mock.couch.utils || require('./couch-utils');

	/* istanbul ignore next */
	var err = function(fn) {
		return function(err) {
			err = typeof err != "string" ? JSON.stringify(err) : err;
			couch.logger.error('[' + fn + '] ' + err);
		};
	};

	var api = {
		data: []
	};

	api.matchFileName = function(file) {
		var matched = null;
		var convert = couch.data.config('static.convert');
		Object.keys(convert).forEach(function(val) {
			if (!couch.utils.isRegExp(val)) {
				return;
			}

			var regExp = new RegExp(val.slice(1, -1));

			if (regExp.test(file)) {
				matched = file.replace(regExp, convert[val]);
			}
		});
		return matched;
	};

	api.convertFileName = function(file) {
		file = file.replace(couch.data.url(), '');
		var matched = this.matchFileName(file);
		var convert = couch.data.config('static.convert');
		switch (true) {
			case (matched != null):
				return matched;
			case (!!convert[file]):
				return convert[file];
		}
		return file;
	}

	/* istanbul ignore next */
	api.downloadPage = function(data) {
		var file = (data.redirect || data.page).replace(couch.data.url(), '');

		if (!file.match(/\.\w{1,4}$/)) {
			file = file + 'index' + couch.data.extension();
		}

		couch.logger.verbose('Fazendo Download da página: ', file);
		grunt.file.write(couch.data.dest() + file, data.body);

		return data;
	};

	api.downloadedAssetIsNewer = function(file, res, date) {

		if (file && !grunt.file.exists(file)) {
			return true;
		}

		var serverDate = new Date(res.headers['date']);
		var serverLastMod = new Date(res.headers['last-modified']);
		var serverDiff = serverDate - serverLastMod;
		var localDate = date || new Date();
		var localLastMod = new Date((fs.statSync(file) || {}).mtime);
		var localDiff = localDate - localLastMod;

		return serverDiff < localDiff;
	};

	/* istanbul ignore next */
	api.downloadAssets = function() {
		couch.logger.log(downloadList.length > 0 ? '\nFazendo download dos assets...' : '\nSem assets para download.');
		var list = downloadList.map(function(val) {
			var dest = val.replace(couch.data.url(), '');

			if (!grunt.option('override-assets') && grunt.file.exists(dest)) {
				couch.logger.verbose('Arquivo ', dest, 'existe. Ignorando...');
				return Promise.resolve();
			}

			return couch.connections.request({url: val, encoding: null})
				.then(function(res) {
					if (this.downloadedAssetIsNewer(dest, res)) {
						if (!grunt.option('verbose')){
							couch.logger.log('.');
						}
						grunt.file.write(dest, res.body);
					}
				}.bind(this))
				.catch(err('downloadAssets'));
		}.bind(this));

		return Promise.all(list).catch(err('downloadAssets'));
	};

	/* istanbul ignore next */
	api.readFiles = function(fileList) {
		var list = fileList.map(function(url) {
			return couch.connections.request(url)
				.then(function(res) {
					var data = {
						page: url,
						body: res.body
					};
					return this.queueAssetsToDownload(data, downloadList);
				}.bind(this));
		}.bind(this));

		return Promise.all(list).catch(err('readFiles'));
	};

	api.queueOriginalImagesToDownload = function(downloadList) {
		downloadList.concat([]).forEach(function(val) {
			if (val.match(/-\d+x\d+\./)) {
				var valOrigin = val.replace(/-\d+x\d+\./, '.');
				if (downloadList.indexOf(valOrigin) < 0) {
					downloadList.push(valOrigin);
				}
			}
		});
	};

	api.queueFilesToRead = function(data, fileList) {
		var fileRegExp = new RegExp(util.format(couch.utils.extensionRegExp, 'css'), "gi"); // TODO: config follow links
		var list = data.body.match(fileRegExp) || [];

		list.forEach(function(val) {
			val = val.slice(1, -1);
			if (fileList.indexOf(val) < 0) {
				fileList.push(val);
			}
		});

		return data;
	};

	api.queueAssetsToDownload = function(data, downloadList) {
		var extensions = couch.data.config('static.download.ext');
		var regExp = new RegExp(util.format(couch.utils.extensionRegExp, extensions.join('|')), 'gmi');
		var list = data.body.match(regExp) || [];

		list.forEach(function(val) {
			val = val.match(/^url\(/) ? val.slice(4, -1) : val.slice(1, -1);
			var finalUrl = url.parse(val);

			delete finalUrl.search;
			delete finalUrl.hash;
			delete finalUrl.query;

			if (data.page) {
				val = url.resolve(data.page, url.format(finalUrl));
			}

			if (downloadList.indexOf(val) < 0) {
				downloadList.push(val);
			}
		});

		return data;
	};

	/* istanbul ignore next */
	api.getPage = function(page) {
		var file = page.replace(couch.data.url(), '');

		return couch.connections.request(page)
			.then(function(req) {
				return {
					page: page,
					body: req.body
				};
			})
			.catch(err('getPage'));
	};

	api.isPageIgnored = function(file, ignoreList) {
		var isIgnored = false;
		if (ignoreList && ignoreList.length) {
			ignoreList.forEach(function(val) {
				val = couch.utils.isRegExp(val) ? new RegExp(val.slice(1, -1)) : val;

				if (file.match(val)) {
					isIgnored = true;
				}
			});
		}
		return isIgnored;
	};

	/* istanbul ignore next */
	api.processPageList = function(pageList) {
		couch.logger.log('\nAcessando páginas...');

		pageList = pageList.map(function(val) {
			var testUrlRegExp = new RegExp(couch.utils.urlToRegExp(couch.data.url()));
			var testUrl = val.replace(testUrlRegExp, '');

			if (this.isPageIgnored(testUrl, couch.data.config('static.ignore'))) {
				return null;
			}

			// primeiro lê todas as páginas e
			// faz o queue de imagens e CSS
			return this.getPage(val)
				.then(function(data) {
					if (!grunt.option('verbose')) {
						couch.logger.log('.');
					}
					return data;
				})
				.catch(err('processPageList'));
		}.bind(this));

		pageList = pageList.filter(function(val) {
			return val != null;
		});

		// faz o download das páginas e imagens
		return Promise.all(pageList).catch(err('task fetch'));
	};

	/* istanbul ignore next */
	api.request = function() {
		this.promise = couch.connections
			.getPageList()

			// get pages
			.then(function(pageList) {
				if (pageList.length === 0) {
					throw new Error([
						'Nenhuma página foi cadastrada no CouchCMS!',
						'Rode "grunt build" para registrá-las.'
					].join('\n'));
				}
				return this.processPageList(pageList);
			}.bind(this))

			// save data locally
			.then(function(data) {
				this.data = data;
				return data;
			}.bind(this))

			// convert paths
			.then(function(data) {
				data.forEach(function(val) {
					val.redirect = this.convertFileName(val.page);
				}.bind(this))
				return data;
			}.bind(this))

			// queue
			.then(function(data) {
				data.forEach(function(val) {
					this.queueAssetsToDownload(val, downloadList);
					this.queueFilesToRead(val, fileList);
				}.bind(this));
				this.queueOriginalImagesToDownload(downloadList);
				return data;
			}.bind(this))

			// read other files (css, etc.) for assets
			.then(function(data) {
				return new Promise(function(ok, fail) {
					this.readFiles(fileList)
						.then(ok.bind(null, data))
						.catch(fail);
				}.bind(this));
			}.bind(this))
			.catch(couch.logger.error);
		return this;
	};

	/* istanbul ignore next */
	api.pipe = function(fnc) {
		this.promise.then(function() {
			if (typeof(fnc) === 'function') {
				this.data = fnc(this.data);
			}
		}.bind(this));
		return this;
	};

	/* istanbul ignore next */
	api.download = function() {
		return this.promise.then(function() {
			if (!grunt.option('assets-only')) {
				this.data.forEach(this.downloadPage.bind(this));
			}
			return this.downloadAssets();
		}.bind(this));
	}

	return api;
};

module.exports = couchFetch();
/* istanbul ignore next */
module.exports.mock = function() {
	return couchFetch.apply(null, arguments);
};
